import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { RestService } from '../rest.service';
import * as _ from 'lodash';
import { UtilService } from '../util.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ValidationService } from '../shared/validation';
import { ROLE, ROLE_USER_TEXT, URL, TEXT, MODAL_ID } from '../shared/constant';
declare var jQuery: any;
import * as moment from 'moment';

@Component({
  selector: 'app-transaction-partner',
  templateUrl: './transaction-partner.component.html',
  styleUrls: ['./transaction-partner.component.css']
})
export class TransactionPartnerComponent implements OnInit {
  public getTransactionUrl: string = '/transactions/partner';
  public data_transaction_partner: any = [];
  public quantityOption = [10, 25, 50, 100]; // quantity item show on a page
  public quantityDefault = 10;// quantity default 10
  public quantity = this.quantityDefault;
  public currentPage = 0;
  public pageArr = [];
  public pageCount;
  public maxEntries;
  public minEntries;
  private currentSort = 'id';
  public maxPageShow = 5;
  public totalCount;
  private isAsc = true;
  public sortOption = {
    id: true,
    username: false,
    amount: false,
    created_at: false
  };

  constructor(
    public validationService: ValidationService,
    private rest: RestService,
    private util: UtilService
  ) { 
    this.getTransactionPartnerList(this.currentPage, this.quantity);
  }

  ngOnInit() {
    
  }

  getTransactionPartnerList(currentPage, quantity, searchText?) {
    this.util.showLoading();
    this.currentPage = currentPage;
    this.quantity = quantity;
    const url = this.util.convertUrl(this.getTransactionUrl, currentPage, quantity, searchText);

    this.rest.GET(url,null, '', false, true).subscribe(res => {
      this.util.hideLoading();
      if (res && res.data && res.data.items) {
        this.data_transaction_partner = res.data.items;
        this.data_transaction_partner.forEach((item, idx) => {
          this.data_transaction_partner[idx].created = moment(this.data_transaction_partner[idx].created_at).format('YYYY/MM/DD HH:mm:ss');
          this.data_transaction_partner[idx].money = this.util.formatCurrency(item.amount);
        })
        this.pageCount = res.data.pageCount;
        this.pageArr = this.util.generatePageArr(this.pageCount, this.maxPageShow, this.currentPage);
        this.totalCount = res.data.totalCount;
        this.minEntries = this.currentPage * this.quantity + 1;
        this.maxEntries = (this.currentPage + 1) * this.quantity > this.totalCount ? this.totalCount : (this.currentPage + 1) * this.quantity;
        // this.sort(this.currentSort);
      }
    }, err => {
      this.util.hideLoading();
      this.util.handleError('', err ? err.message : '');
    })
  }

  sort(type) {
    if (this.sortOption[type]) {
      this.isAsc = !this.isAsc;
      let option = this.isAsc ? 'asc' : 'desc';
      this.data_transaction_partner = _.orderBy(this.data_transaction_partner, [type], [option]);
    } else {
      this.sortOption[this.currentSort] = false;
      this.currentSort = type;
      this.sortOption[type] = true;
      this.data_transaction_partner = _.orderBy(this.data_transaction_partner, [type], ['asc']);
    }
  }

}
