import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransactionPartnerComponent } from './transaction-partner.component';

describe('TransactionPartnerComponent', () => {
  let component: TransactionPartnerComponent;
  let fixture: ComponentFixture<TransactionPartnerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransactionPartnerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionPartnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
