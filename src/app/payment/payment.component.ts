import { Component, OnInit } from '@angular/core';
import { RestService } from '../rest.service';
import * as _ from 'lodash';
import { UtilService } from '../util.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ValidationService } from '../shared/validation';
import { ROLE, STATUS_TEXT, URL, TEXT, MODAL_ID, GENDER_TEXT, GENDER } from '../shared/constant';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
declare var jQuery: any;
import * as moment from 'moment';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {
	public quantityOption = [10, 25, 50, 100]; // quantity item show on a page
	public quantityDefault = 10;// quantity default 10
	public quantity = this.quantityDefault;
	public currentPage = 0;
	private getHistoryPay = URL.HISTORY_PAYMENT;
	private getPayAmount = URL.AMOUNT_PAYMENT;
	private updatePayAmount = URL.UPDATE_PAYMENT;
	public listUser = [];
	public maxPageShow = 5;
	public totalCount;
	public pageArr = [];
	public pageCount;
	public maxEntries;
	public minEntries;
	private currentSort = '_id';
	private isAsc = true;
	public sortOption = {
	_id: true,
	username: false,
	email: false,
	name: false,
	address: false,
	roleUser: false
	};
	public _id = 0;
	public userId = 0;
	public username = "";
	public amount = 0;
	public timerMonth = 0;
	public timerYear = 0;
	public bankname = "";
    public bankaddress = "";
    public bankaccountholder = ""
    public bankaccount = "";
    private status_text = STATUS_TEXT;

	constructor(
		private formBuilder: FormBuilder,
	    public validationService: ValidationService,
	    private rest: RestService,
	    private util: UtilService,
	    private router: Router,
	    private route: ActivatedRoute
	) { 
		this.getListHistory(this.currentPage, this.quantity);
	}

	ngOnInit() {

	}

	getListHistory(currentPage, quantity, searchText?) {
	    this.util.showLoading();
	    this.currentPage = currentPage;
	    this.quantity = quantity;
	    const url = this.util.convertUrlHistory(this.getHistoryPay, currentPage, quantity, searchText);
	    this.rest.GET(url, null, null, null, true).subscribe(res => {
	      this.util.hideLoading();
	      if (res && res.data && res.data.items) {
	        this.listUser = res.data.items;
	        _.forEach(this.listUser, (v, k) => {
	        	v['statusv1'] = v['status'];
	          	v['status'] = this.status_text[v['status']];
	        })
	        this.pageCount = res.data.pageCount;
	        this.pageArr = this.util.generatePageArr(this.pageCount, this.maxPageShow, this.currentPage);
	        this.totalCount = res.data.totalCount;
	        this.minEntries = this.currentPage * this.quantity + 1;
	        this.maxEntries = (this.currentPage + 1) * this.quantity > this.totalCount ? this.totalCount : (this.currentPage + 1) * this.quantity;
	        this.sort(this.currentSort, true);
	      }
	    }, err => {
	      this.util.hideLoading();
	      this.util.handleError('', err ? err.message : '');
	    })
  	}

  	sort(type, isResort?) {
	    isResort && (this.isAsc = !this.isAsc);
	    if (this.sortOption[type]) {
	      this.isAsc = !this.isAsc;
	      let option = this.isAsc ? 'asc' : 'desc';
	      this.listUser = _.orderBy(this.listUser, [type], [option]);
	    } else {
	      this.sortOption[this.currentSort] = false;
	      this.currentSort = type;
	      this.sortOption[type] = true;
	      this.listUser = _.orderBy(this.listUser, [type], ['asc']);
	    }
  	}

  	selectItem(item){
  		this.username = item.username;
  		this._id = item.id;
  		this.userId = item.userId;
		this.amount = 0;
		this.timerMonth = item.monthday;
		this.timerYear = item.yearfull;
		this.bankname = item.bankname;
	    this.bankaddress = item.bankaddress;
	    this.bankaccountholder = item.bankaccountholder;
	    this.bankaccount = item.bankaccount;

	    const url = this.util.convertUrlAmountPay(this.getPayAmount, this.userId, this.timerMonth, this.timerYear);
	    this.rest.GET(url, null, null, null, true).subscribe(res => {
	      this.util.hideLoading();
	      if (res && res.data) {
	        this.amount = res.data.amount;
	        if(res.data.amount == null){
	      		this.amount = 0;	
	      	}
	      }
	    }, err => {
	      this.util.hideLoading();
	      this.util.handleError('', err ? err.message : '');
	    })

  	}

  	approve(){
  		let params = {
  			userId: this.userId,
	        monthday: this.timerMonth,
	        yearfull: this.timerYear,
	        approve: 1,
	        amount : this.amount,
  		}
  		this.rest.POST(URL.UPDATE_PAYMENT, params, null, null, true).subscribe(res => {
  		console.log(res);
        if (res.success) {
        	this.getListHistory(this.currentPage, this.quantity);
        }
      }, err => {
        this.util.hideLoading();
        this.util.handleError('', err ? err.message : '');
      })
  	}

  	reject(){
  		let params_reject = {
  			userId: this.userId,
	        monthday: this.timerMonth,
	        yearfull: this.timerYear,
	        approve: 2,
	        amount : this.amount,
  		}
  		this.rest.POST(URL.UPDATE_PAYMENT, params_reject, null, null, true).subscribe(res => {
  		console.log(res);
        if (res.success) {
        	this.getListHistory(this.currentPage, this.quantity);
        }
      }, err => {
        this.util.hideLoading();
        this.util.handleError('', err ? err.message : '');
      })
  	}


}
