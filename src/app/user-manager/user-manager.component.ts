import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { RestService } from '../rest.service';
import * as _ from 'lodash';
import { UtilService } from '../util.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ValidationService } from '../shared/validation';
import { ROLE, ROLE_USER_TEXT, URL, TEXT, MODAL_ID, GENDER_TEXT, GENDER } from '../shared/constant';
declare var jQuery: any;
import * as moment from 'moment';

@Component({
  selector: 'app-user-manager',
  templateUrl: './user-manager.component.html',
  styleUrls: ['./user-manager.component.css']
})
export class UserManagerComponent implements OnInit {
  @ViewChild('datepicker') date: ElementRef;
  @ViewChild('modal') modal: ElementRef;
  public quantityOption = [10, 25, 50, 100]; // quantity item show on a page
  public quantityDefault = 10;// quantity default 10
  public quantity = this.quantityDefault;
  public currentPage = 0;
  private getUserUrl = URL.GET_USER_LIST;
  public listUser = [];
  public maxPageShow = 5;
  public totalCount;
  public pageArr = [];
  public pageCount;
  public maxEntries;
  public minEntries;
  private currentSort = '_id';
  public createForm: FormGroup;
  public listAutocomplete = [];
  public sortOption = {
    _id: true,
    username: false,
    email: false,
    name: false,
    address: false,
    roleUser: false
  };
  public userOnEdit = {};
  private isAsc = true;
  public role = ROLE;
  private roleUser = ROLE_USER_TEXT;
  public pageNumber = [];
  public searchText = "";
  private timer;
  public gender = GENDER;
  private currentDeleteId;
  constructor(
    private formBuilder: FormBuilder,
    public validationService: ValidationService,
    private rest: RestService,
    private util: UtilService
  ) {
    this.getListUser(this.currentPage, this.quantity);
  }

  ngOnInit() {
    // this.createForm = this.formBuilder.group({
    //   name: ['', [Validators.required]],
    //   gender: ['', [Validators.required]],
    //   username: ['', [Validators.required]],
    //   birthday: ['', [Validators.required]],
    //   email: ['', [Validators.required, this.validationService.checkEmail]],
    //   password: [''],
    //   confirmPassword: [''],
    //   phone: ['', [Validators.required]],
    //   role: ['', [Validators.required]],
    //   province: [''],
    //   address: ['', [Validators.required]],
    //   // saleOff: [''],
    //   // minSale: ['', [this.validationService.onlyNumber]],
    //   // maxSale: ['', [this.validationService.onlyNumber]],
    //   usernameIntroduce: [''],
    //   userIdIntroduce: ['']
    // }, { validator: this.validationService.confirmPassword })
  }

  getListUser(currentPage, quantity, searchText?) {
    this.util.showLoading();
    this.currentPage = currentPage;
    this.quantity = quantity;
    const url = this.util.convertUrl(this.getUserUrl, currentPage, quantity, searchText);
    this.rest.GET(url, null, null, null, true).subscribe(res => {
      this.util.hideLoading();
      if (res && res.data && res.data.items) {
        this.listUser = res.data.items;
        _.forEach(this.listUser, (v, k) => {
          v['roleUser'] = this.roleUser[v['userRoleId']];
        })
        this.pageCount = res.data.pageCount;
        this.pageArr = this.util.generatePageArr(this.pageCount, this.maxPageShow, this.currentPage);
        this.totalCount = res.data.totalCount;
        this.minEntries = this.currentPage * this.quantity + 1;
        this.maxEntries = (this.currentPage + 1) * this.quantity > this.totalCount ? this.totalCount : (this.currentPage + 1) * this.quantity;
        // this.sort(this.currentSort, true);
      }
    }, err => {
      this.util.hideLoading();
      this.util.handleError('', err ? err.message : '');
    })
  }

  sort(type, isResort?) {
    isResort && (this.isAsc = !this.isAsc);
    if (this.sortOption[type]) {
      this.isAsc = !this.isAsc;
      let option = this.isAsc ? 'asc' : 'desc';
      this.listUser = _.orderBy(this.listUser, [type], [option]);
    } else {
      this.sortOption[this.currentSort] = false;
      this.currentSort = type;
      this.sortOption[type] = true;
      this.listUser = _.orderBy(this.listUser, [type], ['asc']);
    }
  }

  ngAfterContentInit() {
    jQuery(this.date.nativeElement).datepicker();
    jQuery(this.date.nativeElement).on('change', () => {
      this.createForm.controls.birthday.setValue(jQuery(this.date.nativeElement).val());
      this.createForm.controls.birthday.updateValueAndValidity();
    })
  }

  search() {
    clearTimeout(this.timer);
    this.timer = setTimeout(() => {
      this.getListUser(0, this.quantity, this.searchText);
    }, 500)
  }

  fillModalData(user) {
    if(user.introducer_id) {
      this.util.showLoading();
      this.rest.GET(URL.GET_USER_LIST + '/' + user.introducer_id, '', null, null, true).subscribe(res => {
        if(res && res.data && res.data.User) {
          this.createForm.controls.usernameIntroduce.setValue(res.data.User.name, { emitEvent: false });
        }
        this.util.hideLoading();
      },
      err => {
        this.util.hideLoading();
      })
    }
    this.userOnEdit = user;
    // reset error
    this.createForm.controls.name.setErrors(null);
    this.createForm.controls.gender.setErrors(null);
    this.createForm.controls.username.setErrors(null);
    this.createForm.controls.birthday.setErrors(null);
    this.createForm.controls.email.setErrors(null);
    this.createForm.controls.phone.setErrors(null);
    this.createForm.controls.password.setErrors(null);
    this.createForm.controls.confirmPassword.setErrors(null);
    this.createForm.controls.role.setErrors(null);
    this.createForm.controls.address.setErrors(null);
    this.createForm.controls.province.setErrors(null);
    // this.createForm.controls.saleOff.setErrors(null);
    this.createForm.controls.userIdIntroduce.setErrors(null);
    //end reset error
    this.createForm.controls.name.setValue(user.name, { emitEvent: false });
    this.createForm.controls.gender.setValue(user.gender, { emitEvent: false });
    this.createForm.controls.username.setValue(user.username, { emitEvent: false });
    this.createForm.controls.birthday.setValue(user.birthday ? moment(user.birthday).format('DD/MM/YYYY') : null, { emitEvent: false });
    this.createForm.controls.email.setValue(user.email, { emitEvent: false });
    this.createForm.controls.phone.setValue(user.phone, { emitEvent: false });
    this.createForm.controls.role.setValue(user.userRoleId, { emitEvent: false });
    this.createForm.controls.address.setValue(user.address, { emitEvent: false });
    this.createForm.controls.province.setValue(user.province, { emitEvent: false });
    // this.createForm.controls.saleOff.setValue(user.saleOff, { emitEvent: false });
    
    this.createForm.controls.userIdIntroduce.setValue(user.introducer_id, { emitEvent: false });
    this.createForm.controls.password.setValue(null, { emitEvent: false });
    this.createForm.controls.confirmPassword.setValue(null, { emitEvent: false });

  }

  updateUser(id) {
    this.validationService.validateAllFields(this.createForm);
    this.createForm.controls.birthday.setValue(jQuery(this.date.nativeElement).val());
    this.createForm.controls.birthday.updateValueAndValidity();
    if (this.createForm.valid) {
      jQuery(this.modal.nativeElement).modal('hide');
      const params = {
        name: this.createForm.controls.name.value,
        gender: parseInt(this.createForm.controls.gender.value),
        birthday: this.createForm.controls.birthday.value ? new Date(this.createForm.controls.birthday.value).getTime() : 0,
        email: this.createForm.controls.email.value,
        phone: this.createForm.controls.phone.value,
        address: this.createForm.controls.address.value,
        username: this.createForm.controls.username.value,
        userRoleId: parseInt(this.createForm.controls.role.value)
        // province: parseInt(this.createForm.controls.province.value),
      };
      this.createForm.controls.password.value &&
        (params['password'] = this.createForm.controls.password.value);
      if (params.userRoleId === 2) {
        // params['saleOff'] = parseFloat(this.createForm.controls.saleOff.value);
        // params['maxSale'] = parseInt(this.createForm.controls.maxSale.value)
        // params['minSale'] = parseInt(this.createForm.controls.minSale.value)
        params['introducer_id'] = parseInt(this.createForm.controls.userIdIntroduce.value)
      } else {
        params['introducer_id'] = 0
        // params['saleOff'] = 0;
        // params['maxSale'] = null;
        // params['minSale'] = null;
      }
      this.util.showLoading();
      this.rest.PUT(URL.GET_USER_LIST + '/' + id, params, null, null, true).subscribe(res => {
        this.util.hideLoading();
        if (res.success) {
          this.util.handleSuccess(TEXT.SUCCESS_TEXT_DESC_UPDATE, TEXT.SUCCESS_TEXT_DESC_UPDATE);
          this.getListUser(this.currentPage, this.quantity);
        } else {
          this.util.handleError('', res ? res.message : '');
        }
      }, err => {
        this.util.hideLoading();
        this.util.handleError('', err ? err.message : '');
      })
    } else {
      this.util.handleError(TEXT.DEFAUL_WARNING, TEXT.ERROR_REQURIRE);
    }
  }


  setCurrentDeleteProduct(id) {
    this.currentDeleteId = id;
  }

  deleteProduct() {
    this.rest.DELETE(URL.GET_USER_LIST + '/' + this.currentDeleteId, null, null, null, true).subscribe(res => {
      this.util.hideLoading();
      if (res.success) {
        this.util.handleSuccess(TEXT.SUCCESS_TEXT_UPDATE, TEXT.SUCCESS_TEXT_DELETE_USER);
        this.getListUser(this.currentPage, this.quantity);
        $('body').removeClass('modal-open');
      } else {
        this.util.handleError('', res ? res.message : '');
      }
    }, err => {
      this.util.hideLoading();
      this.util.handleError('', err ? err.message : '');
    })
  }

  getUserIntroduce() {
    clearTimeout(this.timer);
    this.createForm.controls.userIdIntroduce.setValue("");
    this.timer = setTimeout(() => {
      if (this.createForm.controls.usernameIntroduce.valid && 
          this.createForm.controls.usernameIntroduce.value.length > 2) {
        let name = this.createForm.controls.usernameIntroduce.value;
        let url = this.util.convertUrl(URL.GET_USER_LIST, 0, 10, name); // get 10 user
        this.util.showLoading();
        this.rest.GET(url, null, null, null, true).subscribe(res => {
          this.util.hideLoading();
          if (res && res.data && res.data.items) {
            this.listAutocomplete = res.data.items;
          } else {
            this.listAutocomplete = [];
          }
        }, () => {
          this.util.hideLoading();
          this.listAutocomplete = [];
        })
      }
    }, 200)
  }

  resetListAutocomplete() {
    setTimeout(() => {
      this.listAutocomplete = [];
    }, 500)
  }
  
  selectProducer(name, id) {
    this.createForm.controls.usernameIntroduce.setValue(name);
    this.createForm.controls.userIdIntroduce.setValue(id);
    this.listAutocomplete = [];
  }

}
