export const TEXT = {
  DEFAUL_TITLE: "LỖI MẠNG",
  DEFAUL_ERROR: "LỖI BẮT BUỘC NHẬP",
  DEFAUL_WARNING: "LỖI DỮ LIỆU",
  DEFAUL_DESC: "Vui lòng thử lại sau",
  ERROR_REQURIRE: "Vui lòng nhập hết những trường bắt buộc",
  ERROR_TEXT_LOGIN: "ĐĂNG NHẬP LỖI",
  SUCCESS_TEXT_CREATE: "ĐĂNG KÍ THÀNH CÔNG",
  SUCCESS_TEXT_TITLE: "THÀNH CÔNG",
  SUCCESS_TEXT_DESC: "Đăng kí tài khoản thành công",
  ERROR_TEXT_CREATE: "ĐĂNG KÍ KHÔNG THÀNH CÔNG",
  ERROR_TEXT_DESC: "Đăng kí tài khoản không thành công",
  ERROR_ISEMPTY_DATE: "Bạn cần nhập ngày",
  ERROR_ISEMPTY_DATA: "Không có dữ liệu để xuất",
  SUCCESS_TEXT_UPDATE: "THÀNH CÔNG",
  TITLE_UPDATE_PRODUCT: "Chỉnh sửa hệ sinh thái",
  SUCCESS_TEXT_CREATE_PRODUCT: "Tạo mới hệ sinh thái thành công",
  SUCCESS_TEXT_UPDATE_PRODUCT: "Chỉnh sửa hệ sinh thái thành công",
  SUCCESS_TEXT_DESC_UPDATE: "Chỉnh sửa tài khoản thành công",
  SUCCESS_TEXT_DELETE_CUSTOMER: "Xóa khách hàng thành công",
  SUCCESS_TEXT_UPDATE_CUSTOMER: "Chỉnh sửa  khách hàng thành công",
  SUCCESS_TEXT_CREATE_CUSTOMER: "Tạo mới khách hàng thành công",
  SUCCESS_TEXT_DELETE_PRODUCT: "Xóa hệ sinh thái thành công",
  SUCCESS_TEXT_UPDATE_CATE: "Chỉnh sửa danh mục thành công",
  SUCCESS_TEXT_CREATE_CATE: "Tạo mới danh mục thành công",
  SUCCESS_TEXT_DELETE_CATE: "Xóa danh mục thành công",
  SUCCESS_TEXT_UPDATE_PARTNER: "Chỉnh sửa đối tác thành công",
  SUCCESS_TEXT_CREATE_PARTNER: "Tạo mới đối tác thành công",
  SUCCESS_TEXT_DELETE_PARTNER: "Xóa đối tác thành công",
  SUCCESS_TEXT_UPDATE_ADS: "Chỉnh sửa quảng cáo thành công",
  SUCCESS_TEXT_CREATE_ADS: "Tạo mới quảng cáo thành công",
  SUCCESS_TEXT_DELETE_ADS: "Xóa quảng cáo thành công",
  SUCCESS_TEXT_DELETE_USER: "Xóa tài khoản thành công",
  SUCCESS_TEXT_UPDATE_CONFIG: "Chỉnh sửa thành công",
  SUCCESS_TEXT_UPDATE_PROMOTIONS: "Chỉnh sửa khuyến mãi thành công",
  SUCCESS_TEXT_CREATE_PROMOTIONS: "Tạo mới khuyến mãi thành công",
  SUCCESS_TEXT_DELETE_PROMOTIONS: "Xóa khuyến mãi thành công",
  SUCCESS_TEXT_UPDATE_PROMOTIONS_APPROVE: "Chỉnh sửa khuyến mãi thành công",
  SUCCESS_TEXT_CREATE_PROMOTIONS_APPROVE: "Tạo mới khuyến mãi thành công",
  SUCCESS_TEXT_DELETE_PROMOTIONS_APPROVE: "Xóa khuyến mãi thành công",
  SUCCESS_TEXT_CREATE_LINK_PRODUCT: "Tạo mới chủ doanh nghiệp thành công",
  SUCCESS_TEXT_UPDATE_LINK_PRODUCT: "Chỉnh sửa chủ doanh nghiệp thành công",
  SUCCESS_TEXT_DELETE_LINK_PRODUCT: "Xoá chủ doanh nghiệp thành công",
}
export const URL = {
  BASE_URL : "https://pacificgo.asia/secret",
  BASE_URL_ADMIN : "https://pacificgo.asia/admin",
  GET_USER_LIST: "/users",
  GET_USER_LIST_ROLE: "/users/roleId",
  GET_TRANS_LIST_PARTNER: "/transactions/partner/getTrans",
  GET_TRANS_AMOUNT_MONTH: "/transactions/partner/getTransMonth",
  GET_TRANS_LIST: "/transactions/commit",
  LOGIN: "/login",
  PRODUCT_LIST: "/products",
  CATEGORY_LIST: "/categories?paginate=0&perPage=10",
  ALL_CATEGORIES : "/categories",
  ALL_PROVINCES : "/provinces/all",
  GET_WARD: "wards",
  UPLOAD: "https://pacificgo.asia/admin/upload",
  URL_IMAGE: "https://pacificgo.asia/admin/uploads",
  CREATE_PRODUCT: "/products",
  GET_CUSTOMER_LIST: "/customers",
  REMOVE_CUSTOMER: "/customers/",
  GET_TRANSACTION_ADMIN: "/transactions/discount-partner",
  GET_TRANS_AMOUNT: "/transactions/partner/getAmount",
  LOGOUT: "/logout",
  GET_CATEGORIES: "/categories",
  GET_SUB_CATEGORIES: "/subcategories/",
  SUB_CATEGORY: "/subcategories",
  PARTNER: "/partners",
  ADS: "/advertisements",
  CONFIG: "/config",
  PROMOTIONS: "/promotions",
  PROMOTIONS_APPROVE: "/promotions/transactions",
  PRODUCT_DRAF: "/trash-products",
  GET_LINK_PRODUCT: "/link/product",
  UPDATE_LINK_PRODUCT: "/link/edit",
  HISTORY_PAYMENT : "/user/listpayment",
  AMOUNT_PAYMENT : "/payment/amount",
  UPDATE_PAYMENT : "/payment/update",
}
export const MODAL_ID = {
  ERROR: "#errorModal",
  LOADING: "#preloader",
  ERROR_TITLE: "#exampleModalLabel",
  ERORR_DESC: "#exampleModalDesc",
  UPDATE_USER_MODAL: "#Modal1",
  CREATE_USER_MODAL: "#Modal2",
}

export const KEY_STORAGE = {
  TOKEN: "pacific_token",
  USER_INFOR: "user_infor",
  LOGIN_TIME: "login_time",
  EXPIRY_TIME: "expiry_time"
}

export const ROLE_USER = {
  ADMIN: 1,
  PARTNER: 2,
  CUSTOMER: 3
}

export const ROLE = [
  {
    code: 1,
    text: "Admin"
  },
  {
    code: 2,
    text: "Đối tác"
  },
  {
    code: 3,
    text: "Khách hàng"
  },
  {
    code: 4,
    text: "Thành viên"
  },
]

export const ROLE_USER_TEXT = {
  1: "Admin",
  2: "Đối tác",
  3: "Khách hàng",
  4: "Thành viên"
}

export const STATUS_TEXT = {
  0: "Đang xử lý",
  1: "Đã duyệt",
  2: "Từ chối",
}

export const GENDER = [
  {
    code: 1,
    text: "Nam",
  },
  {
    code: 2,
    text: "Nữ",
  },
  {
    code: 1,
    text: "LGBT",
  }
]

export const GENDER_TEXT = {
  1: "Nam",
  2: "Nữ",
  3: "LGBT"
}

export const GOOGLE_MAP_API_KEY = "AIzaSyDb3gpTsT1HL_v3wsjTUU54wj0e6_sxrBA";

export const INTRODUCE_LIST = [
  {
    code: 1,
    text: "Phòng riêng"
  },
  {
    code: 2,
    text: "Ghế trẻ em"
  },
  {
    code: 3,
    text: "Chỗ hút thuốc"
  },
  {
    code: 4,
    text: "HD trực tiếp"
  },
  {
    code: 5,
    text: "Ngồi ngoài trời"
  },
  {
    code: 6,
    text: "Chỗ chơi trẻ em"
  },
  {
    code: 7,
    text: "Wifi"
  },
  {
    code: 8,
    text: "Ship món về"
  },
  {
    code: 9,
    text: "Visa / Master card"
  },
  {
    code: 10,
    text: "Hoá đơn đỏ"
  },
  {
    code: 11,
    text: "Set Lunch"
  },
  {
    code: 12,
    text: "Nên đặt truớc"
  },
  {
    code: 13,
    text: "Có bàn ngoài trời"
  },
  {
    code: 14,
    text: "Có hỗ trợ nguời khuyết tật"
  },
  {
    code: 15,
    text: "Có nhạc sống"
  },
  {
    code: 16,
    text: "Có chỗ cho trẻ"
  },
  {
    code: 17,
    text: "Giữ xe miễn phí"
  },
  {
    code: 18,
    text: "Sân tennis"
  },
  {
    code: 19,
    text: "Có karaoke"
  },
  {
    code: 20,
    text: "Có lò suởi"
  },
  {
    code: 21,
    text: "Có thẻ thành viên"
  },
  {
    code: 22,
    text: "Hồ bơi"
  },
  {
    code: 23,
    text: "Bowling"
  },
  {
    code: 24,
    text: "Điều hoà"
  }
]
export const LINK_CONFIG = [
  {
    code: 1,
    text: "Link cột 1"
  },
  {
    code: 2,
    text: "Link cột 2"
  },
  {
    code: 3,
    text: "Link cột Sosical"
  },
  {
    code: 4,
    text: "Link Footer"
  },
]