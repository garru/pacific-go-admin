import { Component, OnInit, Query, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { RestService } from '../rest.service';
import { UtilService } from '../util.service';
import { URL, TEXT, GENDER, ROLE } from '../shared/constant';
import { ValidationService } from '../shared/validation';
import * as $ from 'jquery';
import * as moment from 'moment';
import { parseHostBindings } from '@angular/compiler';
declare var jQuery: any;

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {
  @ViewChild('datepicker') date: ElementRef;
  public createForm: FormGroup;
  public role = ROLE;
  public gender = GENDER;
  private timer;
  public listAutocomplete = [];
  constructor(
    private formBuilder: FormBuilder,
    public validationService: ValidationService,
    private rest: RestService,
    private util: UtilService
  ) {
    this.createForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      username: ['', [Validators.required]],
      password: ['', [Validators.required]],
      confirmPassword: [''],
      gender: [1, [Validators.required]],
      birthday: ['', Validators.required],
      email: ['', [Validators.required, validationService.checkEmail]],
      phone: ['', [Validators.required]],
      role: [1, [Validators.required]],
      province: [1, [Validators.required]],
      address: ['', [Validators.required]],
      // minSale: ['', [validationService.onlyNumber]],
      // maxSale: ['', [validationService.onlyNumber]],
      saleOff: ['', [validationService.onlyNumber]],
      userIdIntroduce: [''],
      usernameIntroduce: ['']
    }, { validator: this.validationService.confirmPassword })
  }

  ngOnInit() {
  }

  ngAfterContentInit() {
    jQuery(this.date.nativeElement).datepicker();
    jQuery(this.date.nativeElement).datepicker({
      autoclose: true,
      todayHighlight: true
    });
    jQuery(this.date.nativeElement).on('change', () => {
      this.createForm.controls.birthday.setValue(jQuery(this.date.nativeElement).val());
      this.createForm.controls.birthday.updateValueAndValidity();
    })
  }

  createUser() {
    // this.createForm.controls.confirmPassword.setValidators([Validators.required, this.validationService.checkEmail])
    this.validationService.validateAllFields(this.createForm);
    if (this.createForm.valid) {
      const params = {
        name: this.createForm.controls.name.value,
        username: this.createForm.controls.username.value,
        password: this.createForm.controls.password.value,
        gender: this.createForm.controls.gender.value,
        birthday: moment(this.createForm.controls.birthday.value).valueOf(),
        email: this.createForm.controls.email.value,
        phone: this.createForm.controls.phone.value,
        // province: this.createForm.controls.province.value,
        address: this.createForm.controls.address.value,
        userRoleId: parseInt(this.createForm.controls.role.value)
      }
      if (params.userRoleId === 2) {
        params['introducer_id'] = parseInt(this.createForm.controls.userIdIntroduce.value)
        // params['maxSale'] = parseInt(this.createForm.controls.maxSale.value)
        // params['minSale'] = parseInt(this.createForm.controls.minSale.value)
      } else {
        params['introducer_id'] = null;
        // params['maxSale'] = null;
        // params['minSale'] = null;
      }
      params['otp_validate'] = 1;
      this.util.showLoading();
      this.rest.POST(URL.GET_USER_LIST, params, null, null, true).subscribe(res => {
        this.util.hideLoading();
        if (res.success) {
          this.util.handleSuccess();
        } else {
          this.util.handleError(res.message, res.message);
        }
      }, err => {
        this.util.hideLoading();
        this.util.handleError();
      })
    } else {
      this.util.handleError(TEXT.DEFAUL_WARNING, TEXT.ERROR_REQURIRE);
    }
  }

  getUserIntroduce() {
    clearTimeout(this.timer);
    this.createForm.controls.userIdIntroduce.setValue("");
    this.timer = setTimeout(() => {
      if (this.createForm.controls.usernameIntroduce.valid && 
          this.createForm.controls.usernameIntroduce.value.length > 2) {
        let name = this.createForm.controls.usernameIntroduce.value;
        let url = this.util.convertUrl(URL.GET_USER_LIST, 0, 10, name); // get 10 user
        this.util.showLoading();
        this.rest.GET(url, null, null, null, true).subscribe(res => {
          this.util.hideLoading();
          if (res && res.data && res.data.items) {
            this.listAutocomplete = res.data.items;
          } else {
            this.listAutocomplete = [];
          }
        }, () => {
          this.util.hideLoading();
          this.listAutocomplete = [];
        })
      }
    }, 200)
  }

  selectProducer(name, id) {
    this.createForm.controls.usernameIntroduce.setValue(name);
    this.createForm.controls.userIdIntroduce.setValue(id);
    this.listAutocomplete = [];
  }

  resetListAutocomplete() {
    setTimeout(() => {
      this.listAutocomplete = [];
    }, 500)
  }

}
