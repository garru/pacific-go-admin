import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { RestService } from '../rest.service';
import * as _ from 'lodash';
import { UtilService } from '../util.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ValidationService } from '../shared/validation';
import { ROLE, ROLE_USER_TEXT, URL, TEXT, MODAL_ID } from '../shared/constant';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
declare var jQuery: any;
import * as moment from 'moment';


@Component({
  selector: 'app-transaction-pacific',
  templateUrl: './transaction-pacific.component.html',
  styleUrls: ['./transaction-pacific.component.css']
})
export class TransactionPacificComponent implements OnInit {
  public getTransactionPacificUrl: string = '/transactions/commitPartner';
  public listTrans: any = [];
  public listTrans_total = [];
  public quantityOption = [10, 25, 50, 100]; // quantity item show on a page
  public quantityDefault = 10;// quantity default 10
  public quantity = this.quantityDefault;
  public currentPage = 0;
  public pageArr = [];
  public pageCount;
  public maxEntries;
  public minEntries;
  private currentSort = 'id';
  public maxPageShow = 5;
  public totalCount;
  private isAsc = true;
  public sortOption = {
    partnerName: false,
    commission: false,
    created_at: false,
    pacific_amount: false
  };
  private timer;
  public amount = 0;
  public searchText = "";
  constructor(
          private formBuilder: FormBuilder,
          public validationService: ValidationService,
          private rest: RestService,
          private util: UtilService,
          private router: Router,
          private route: ActivatedRoute
  ) {
    this.getTransactionPartnerList(this.currentPage, this.quantity);
  }

  ngOnInit() {
  }

  getTransactionPartnerList(currentPage, quantity, searchText?) {
    this.util.showLoading();
    this.currentPage = currentPage;
    this.quantity = quantity;
    const url = this.util.convertUrl(this.getTransactionPacificUrl, currentPage, quantity, searchText);
    this.rest.GET(url, null, null, null, true).subscribe(res => {
      this.util.hideLoading();
      if (res && res.data) {
          this.listTrans = res.data.items;
          this.pageCount = res.data.pageCount;
          this.pageArr = this.util.generatePageArr(this.pageCount, this.maxPageShow, this.currentPage);
          this.totalCount = res.data.totalCount;
          this.minEntries = this.currentPage * this.quantity + 1;
          this.maxEntries = (this.currentPage + 1) * this.quantity > this.totalCount ? this.totalCount : (this.currentPage + 1) * this.quantity;
          if(res.data.amount == ''){
            this.amount = 0
          }else{
            this.amount = res.data.amount
          }
          
          // this.sort(this.currentSort, true);
      } else {
        this.util.handleError('', res ? res.message : '');
      }
    }, err => {
      this.util.hideLoading();
      this.util.handleError('', err ? err.message : '');
    })
  }

  sort(type) {
    if (this.sortOption[type]) {
      this.isAsc = !this.isAsc;
      let option = this.isAsc ? 'asc' : 'desc';
      this.listTrans = _.orderBy(this.listTrans, [type], [option]);
    } else {
      this.sortOption[this.currentSort] = false;
      this.currentSort = type;
      this.sortOption[type] = true;
      this.listTrans = _.orderBy(this.listTrans, [type], ['asc']);
    }
  }

  /*changePage(idx, quantity) {
    this.currentPage = idx;
    this.quantity = quantity;
    this.pageCount = Math.ceil(this.totalCount / this.quantity);
    this.pageArr = this.util.generatePageArr(this.pageCount, this.maxPageShow, this.currentPage);
    this.minEntries = this.currentPage * this.quantity + 1;
    this.maxEntries = (this.currentPage + 1) * this.quantity > this.totalCount ? this.totalCount : (this.currentPage + 1) * this.quantity;
    this.listTrans = this.listTrans_total.filter((item, index) => {
      return (index >= idx * quantity) && (index < (idx + 1) * quantity);
    });
  }*/

  search() {
    clearTimeout(this.timer);
    this.timer = setTimeout(() => {
      this.getTransactionPartnerList(0, this.quantity, this.searchText);
    }, 500)
  }

}
