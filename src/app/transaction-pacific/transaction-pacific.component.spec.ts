import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransactionPacificComponent } from './transaction-pacific.component';

describe('TransactionPacificComponent', () => {
  let component: TransactionPacificComponent;
  let fixture: ComponentFixture<TransactionPacificComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransactionPacificComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionPacificComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
