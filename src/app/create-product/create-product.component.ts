import { Component, OnInit, ViewChild, ElementRef, NgZone } from '@angular/core';
import { INTRODUCE_LIST } from '../shared/constant';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { RestService } from '../rest.service';
import { UtilService } from '../util.service';
import { URL, TEXT, GENDER, ROLE } from '../shared/constant';
import { ValidationService } from '../shared/validation';
import * as _ from 'lodash';
declare var jQuery: any;
import { FileUploader, FileItem, ParsedResponseHeaders } from "ng2-file-upload";
import { } from 'googlemaps';
import { MapsAPILoader } from '@agm/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-product',
  templateUrl: './create-product.component.html',
  styleUrls: ['./create-product.component.css']
})
export class CreateProductComponent implements OnInit {
  @ViewChild("open") open: ElementRef;
  @ViewChild("close") close: ElementRef;
  @ViewChild("search")
  public searchElementRef: ElementRef;
  public aboutMoreData = INTRODUCE_LIST;
  public aboutMoreList = {};
  public createForm: FormGroup;
  public role = ROLE;
  public gender = GENDER;
  public categories = [];
  public currentCategory = [];
  public cityList = [];
  public wardList = [];
  public hasWardList = false;
  public hasSubCategories = false;
  public uploader: FileUploader = new FileUploader({ url: URL.UPLOAD, autoUpload: true, removeAfterUpload: true });
  public currentFileType = '';
  public listFile = {
    productImage: [],
    menuImage: [],
    avatar: []
  }
  public count = {
    productImage: 0
  }
  public latitude: number;
  public longitude: number;
  public zoom: number;
  public saleDetail;
  public aboutDetail;
  private userInfor = this.util.getUserInfor();
  private timer;
  public listAutocomplete = [];
  private user_id : number;

  constructor(
    private formBuilder: FormBuilder,
    public validationService: ValidationService,
    private rest: RestService,
    private util: UtilService,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    private router: Router
  ) {

    this.createForm = this.formBuilder.group({
      categoryId: [null, [Validators.required]],
      productTitle: [null, [Validators.required]],
      slug: [null],
      city: [null, [Validators.required]],
      ward: [null, Validators.required],
      openStore: [null, [Validators.required]],
      closeStore: [null, [Validators.required]],
      attribute: [null, [Validators.required]],
      address: [null, [Validators.required]],
      priceLow: [null, [validationService.onlyNumber]],
      pricehight: [null, [Validators.required, validationService.onlyNumber]],
      phone: ['', [Validators.required, validationService.onlyNumber]],
      productImage: [null, [Validators.required]],
      productTN: [null, [Validators.required]],
      minSale: [null, [Validators.required, validationService.onlyNumber]],
      maxSale: [null, [validationService.onlyNumber]],
      saleDetail: [null, [Validators.required]],
      aboutMore: [null],
      aboutDetail: [null, [Validators.required]],
      menuImage: [null],
      subCategoryId: [null, [Validators.required]],
      rating: [null, [Validators.required, validationService.onlyNumber]],
      is_top: [null],
      reputation: [null],
      label1: [null],
      label2: [null],
      label3: [null],
      label4: [null],
      label5: [null]
    });

    this.createForm.controls.categoryId.valueChanges.subscribe(id => {
      this.createForm.controls.subCategoryId.setValue(null);
      this.currentCategory = [];
      let temp = this.categories.find(item => item.id === parseInt(id));
      if (temp && temp.sub_category_ids) {
        let subId = temp.sub_category_ids.split(",");
        let subName = temp.sub_category_names.split(",");
        subId.forEach((item, idx) => {
          let cate = {
            code: item,
            text: subName[idx]
          }
          this.currentCategory.push(cate);
        })
        this.hasSubCategories = true
      } else {
        this.currentCategory = [];
        this.hasSubCategories = false;
      }
    })
    this.createForm.controls.city.valueChanges.subscribe(id => {
      this.util.showLoading();
      this.rest.GET('', '', URL.BASE_URL + '/provinces/' + id + '/' + URL.GET_WARD, null, true).subscribe(res => {
        this.util.hideLoading();
        this.createForm.controls.ward.setValue(null);
        if (res && res.data && res.data.items) {
          this.wardList = res.data.items;
          this.hasWardList = true;
        } else {
          this.wardList = [];
          this.hasWardList = false;
        }
      }, err => {
        this.util.hideLoading();
        this.util.handleError('', err ? err.message : '');
      })
    })
    this.util.showLoading();
    this.rest.GET('', '', URL.BASE_URL + URL.ALL_CATEGORIES, null, true).subscribe(categories => {
      if (categories && categories && categories.data) {
        this.categories = categories.data.items;
        this.rest.GET(URL.ALL_PROVINCES, '', '', null, true).subscribe(cityList => {
          if (cityList && cityList.data) {
            this.cityList = cityList.data;
          } else {

          }
        })
        this.util.hideLoading();
      } else {
        this.util.hideLoading();
        this.util.handleError('', categories ? categories.message : '');
      }
    }, err => {
      this.util.hideLoading();
      this.util.handleError('', err ? err.message : '');
    })
    this.uploader.onAfterAddingFile = (file) => { file.withCredentials = false; }
    this.uploader.onErrorItem = (item, response, status, headers) => this.onErrorItem(item, response, status, headers);
    this.uploader.onSuccessItem = (item, response, status, headers) => this.onSuccessItem(item, response, status, headers);
  }

  ngOnInit() {
    this.zoom = 16;
    this.latitude = 39.8282;
    this.longitude = -98.5795;
    this.setCurrentPosition();
    this.mapsAPILoader.load().then(() => {
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ["address"]
      });
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();
          this.createForm.controls.address.setValue(jQuery(this.searchElementRef.nativeElement).val());
          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          //set latitude, longitude and zoom
          this.latitude = place.geometry.location.lat();
          this.longitude = place.geometry.location.lng();
          this.zoom = 16;
        });
      });
    });
  }

  createProduct() {
    let productImage = [];
    let menuImage = [];
    let productTN = [];
    let aboutMore = [];
    this.listFile.productImage.forEach(image => {
      productImage.push(image.url);
    })
    this.createForm.controls.productImage.setValue(productImage.length ? JSON.stringify(productImage) : '', { emitEvent: false });
    this.listFile.menuImage.forEach(image => {
      menuImage.push(image.url);
    })
    this.createForm.controls.menuImage.setValue(menuImage.length ? JSON.stringify(menuImage) : '', { emitEvent: false });
    this.listFile.avatar.forEach(image => {
      productTN.push(image.url);
    })
    this.createForm.controls.productTN.setValue(productTN[0], { emitEvent: false });
    this.createForm.controls.saleDetail.setValue(this.saleDetail, { emitEvent: false });
    this.createForm.controls.aboutDetail.setValue(this.aboutDetail, { emitEvent: false });
    _.forEach(this.aboutMoreList, (value, key) => {
      let about = this.aboutMoreData.find(item => value && (item.code === parseInt(key)));
      about && aboutMore.push(about.text);
    })
    this.createForm.controls.aboutMore.setValue(JSON.stringify(aboutMore), { emitEvent: false });
    this.validationService.validateAllFields(this.createForm);
    console.log(this.createForm.value);
    if (this.createForm.valid) {
      let params = {
        productTitle: this.createForm.controls.productTitle.value,
        slug: this.generateSlug(this.createForm.controls.productTitle.value),
        categoryId: parseInt(this.createForm.controls.categoryId.value),
        sub_categories_id: parseInt(this.createForm.controls.subCategoryId.value),
        openStore: this.createForm.controls.openStore.value,
        closeStore: this.createForm.controls.closeStore.value,
        attribute: this.createForm.controls.attribute.value,
        address: this.createForm.controls.address.value,
        productImage: this.createForm.controls.productImage.value,
        minSale: this.createForm.controls.minSale.value,
        maxSale: this.createForm.controls.maxSale.value,
        saleDetail: this.createForm.controls.saleDetail.value,
        menuImage: this.createForm.controls.menuImage.value,
        productTN: this.createForm.controls.productTN.value,
        aboutDetail: this.createForm.controls.aboutDetail.value,
        aboutMore: this.createForm.controls.aboutMore.value,
        priceLow: this.createForm.controls.priceLow.value || _.isNumber(this.createForm.controls.priceLow.value) ? this.createForm.controls.priceLow.value.toString() : '',
        pricehight: this.createForm.controls.pricehight.value || _.isNumber(this.createForm.controls.pricehight.value) ? this.createForm.controls.pricehight.value.toString() : '',
        city: parseInt(this.createForm.controls.city.value),
        ward: parseInt(this.createForm.controls.ward.value),
        lat: this.latitude,
        lng: this.longitude,
        reputation: this.createForm.controls.reputation.value ? 1 : 0,
        is_top: this.createForm.controls.is_top.value ? 1 : 0,
        rating: this.createForm.controls.rating.value || 0,
        phone: this.createForm.controls.phone.value || '',
        // user_id: this.user_id
      }
      this.util.showLoading();
      this.rest.POST(URL.CREATE_PRODUCT, params, null, null, true).subscribe(res => {
        this.util.hideLoading();
        if (res.success) {
          this.util.handleSuccess('', TEXT.SUCCESS_TEXT_CREATE_PRODUCT);
          this.router.navigate(['/product-manager']);
        }
      }, err => {
        this.util.hideLoading();
        this.util.handleError('', err ? err.message : '');
      })

    } else {
      this.util.handleError(TEXT.DEFAUL_WARNING, TEXT.ERROR_REQURIRE);
    }
  }

  onSuccessItem(item: FileItem, response: string, status: number, headers: ParsedResponseHeaders): any {
    //this gets triggered only once when first file is uploaded
    item.file['url'] = URL.URL_IMAGE + '/' + JSON.parse(item._xhr.response).data[0].filename;
    (this.currentFileType !== 'productImage' && this.currentFileType !== 'menuImage') && (this.listFile[this.currentFileType] = []);
    this.listFile[this.currentFileType].push(item.file);
    if (item['uploader'].queue.length === 1) {
      this.util.hideLoading();
    }
  }

  onErrorItem(item: FileItem, response: string, status: number, headers: ParsedResponseHeaders): any {
    if (response) {
      let error = JSON.parse(response); //error server response
      if (item['uploader'].queue.length === 1) {
        this.util.hideLoading();
      }
    } else {
      this.util.hideLoading();
      this.util.handleError();
    }
  }

  onFileSelected(type) {
    this.currentFileType = type;
    this.util.showLoading();
  }

  setCurrentPosition() {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 16;
      });
    }
  }

  generateSlug(title: string) {
    title = this.util.formatUnicode(title);
    return title.toLowerCase().split(' ').join('-');
  }

  ngAfterContentInit() {
  }

  removeImage(type, i) {
    this.listFile[type].splice(i, 1);
  }
  getUserIntroduce() {
    clearTimeout(this.timer);
    this.timer = setTimeout(() => {
      if (this.createForm.controls.userName.valid && 
          this.createForm.controls.userName.value.length > 2) {
        let name = this.createForm.controls.userName.value;
        let url = this.util.convertUrl(URL.GET_USER_LIST, 0, 10, name); // get 10 user
        this.util.showLoading();
        this.rest.GET(url, null, null, null, true).subscribe(res => {
          this.util.hideLoading();
          if (res && res.data && res.data.items) {
            this.listAutocomplete = res.data.items;
          } else {
            this.listAutocomplete = [];
          }
        }, () => {
          this.util.hideLoading();
          this.listAutocomplete = [];
        })
      }
    }, 200)
  }
  // selectProducer(name, id) {
  //   this.createForm.controls.userName.setValue(name);
  //   this.user_id = id;
  //   this.listAutocomplete = [];
  // }

  // resetListAutocomplete() {
  //   setTimeout(() => {
  //     this.listAutocomplete = [];
  //   }, 500)
  // }

}
