import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserPartnerComponent } from './user-partner.component';

describe('UserPartnerComponent', () => {
  let component: UserPartnerComponent;
  let fixture: ComponentFixture<UserPartnerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserPartnerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserPartnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
