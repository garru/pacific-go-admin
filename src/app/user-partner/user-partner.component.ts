import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { RestService } from '../rest.service';
import * as _ from 'lodash';
import { UtilService } from '../util.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ValidationService } from '../shared/validation';
import { ROLE, ROLE_USER_TEXT, URL, TEXT, MODAL_ID, GENDER_TEXT, GENDER } from '../shared/constant';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
declare var jQuery: any;
import * as moment from 'moment';
import 'chart.js';

@Component({
  selector: 'app-user-partner',
  templateUrl: './user-partner.component.html',
  styleUrls: ['./user-partner.component.css']
})
export class UserPartnerComponent implements OnInit {
	@ViewChild('fromdate') fromdate: ElementRef;
	@ViewChild('todate') todate: ElementRef;
	@ViewChild('modal') modal: ElementRef;
	public quantityOption = [10, 25, 50, 100]; // quantity item show on a page
	public quantityDefault = 10;// quantity default 10
	public quantity = this.quantityDefault;
	public currentPage = 0;
	private getUserUrl = URL.GET_USER_LIST_ROLE;
	private getAmount = URL.GET_TRANS_AMOUNT;
	public listUser = [];
	public maxPageShow = 5;
	public totalCount;
	public pageArr = [];
	public pageCount;
	public maxEntries;
	public minEntries;
	private currentSort = '_id';
	public createForm: FormGroup;
	public sortOption = {
	_id: true,
	username: false,
	email: false,
	name: false,
	address: false,
	roleUser: false
	};
	public userOnEdit = {};
	private isAsc = true;
	public role = ROLE;
	private roleUser = ROLE_USER_TEXT;
	public pageNumber = [];
	public searchText = "";
	private timer;
	public gender = GENDER;
	private currentDeleteId;
	private _id = 0;
	private fromdateVal = '';
	private todateVal = '';
	private roleId = '';
	private title = '';
	

	public lineChartData:Array<any> = [
	    {data: [0, 0, 0, 0 , 0, 0, 0], label: 'Tiền'}
  	];
  	public lineChartLabels:Array<any> = [];
	public lineChartOptions:any = {
		responsive: true
	};
	public lineChartColors:Array<any> = [
		{ 
		  backgroundColor: 'rgba(148,159,177,0.2)',
		  borderColor: 'rgba(148,159,177,1)',
		  pointBackgroundColor: 'rgba(148,159,177,1)',
		  pointBorderColor: '#fff',
		  pointHoverBackgroundColor: '#fff',
		  pointHoverBorderColor: 'rgba(148,159,177,0.8)'
		},
		
	];
	public lineChartLegend:boolean = true;
	public lineChartType:string = 'line';
	 
	public randomize():void {
		let _lineChartData:Array<any> = new Array(this.lineChartData.length);
		for (let i = 0; i < this.lineChartData.length; i++) {
			_lineChartData[i] = {data: new Array(this.lineChartData[i].data.length), label: this.lineChartData[i].label};
	  		for (let j = 0; j < this.lineChartData[i].data.length; j++) {
	    		_lineChartData[i].data[j] = Math.floor((Math.random() * 100) + 1);
	  		}
		}
		this.lineChartData = _lineChartData;
	}
	 
	// events
	public chartClicked(e:any):void {
		console.log(e);
	}
	 
	public chartHovered(e:any):void {
		console.log(e);
	}

	private paramsSubscription;
	constructor(
	    private formBuilder: FormBuilder,
	    public validationService: ValidationService,
	    private rest: RestService,
	    private util: UtilService,
	    private router: Router,
	    private route: ActivatedRoute
  	) {
  		this.roleId = this.route.snapshot.data['roleId'];
  		this.title = this.route.snapshot.data['title'];
    	this.getListUser(this.currentPage, this.quantity);
  	}


  	ngOnInit() {
  		
		this.createForm = this.formBuilder.group({
			name: ['', [Validators.required]],
			gender: ['', [Validators.required]],
			username: ['', [Validators.required]],
			birthday: ['', [Validators.required]],
			fromdate: ['', [Validators.required]],
			todate: ['', [Validators.required]],
			email: ['', [Validators.required, this.validationService.checkEmail]],
			password: [''],
			confirmPassword: [''],
			phone: ['', [Validators.required]],
			role: ['', [Validators.required]],
			province: [''],
			address: ['', [Validators.required]],
			saleOff: [''],
			userIdIntroduce: ['']
		}, { validator: this.validationService.confirmPassword })
  	}

  	getListUser(currentPage, quantity, searchText?) {
	    this.util.showLoading();
	    this.currentPage = currentPage;
	    this.quantity = quantity;
	    const url = this.util.convertUrlRole(this.getUserUrl, this.roleId, currentPage, quantity, searchText);
	    this.rest.GET(url, null, null, null, true).subscribe(res => {
	      this.util.hideLoading();
	      if (res && res.data && res.data.items) {
	        this.listUser = res.data.items;
	        _.forEach(this.listUser, (v, k) => {
	          v['roleUser'] = this.roleUser[v['userRoleId']];
	        })
	        this.pageCount = res.data.pageCount;
	        this.pageArr = this.util.generatePageArr(this.pageCount, this.maxPageShow, this.currentPage);
	        this.totalCount = res.data.totalCount;
	        this.minEntries = this.currentPage * this.quantity + 1;
	        this.maxEntries = (this.currentPage + 1) * this.quantity > this.totalCount ? this.totalCount : (this.currentPage + 1) * this.quantity;
	        // this.sort(this.currentSort, true);
	      }
	    }, err => {
	      this.util.hideLoading();
	      this.util.handleError('', err ? err.message : '');
	    })
  	}

  	sort(type, isResort?) {
	    isResort && (this.isAsc = !this.isAsc);
	    if (this.sortOption[type]) {
	      this.isAsc = !this.isAsc;
	      let option = this.isAsc ? 'asc' : 'desc';
	      this.listUser = _.orderBy(this.listUser, [type], [option]);
	    } else {
	      this.sortOption[this.currentSort] = false;
	      this.currentSort = type;
	      this.sortOption[type] = true;
	      this.listUser = _.orderBy(this.listUser, [type], ['asc']);
	    }
  	}

  	ngAfterContentInit() {
	    
	    jQuery(this.fromdate.nativeElement).datepicker();
	    jQuery(this.fromdate.nativeElement).on('change', () => {
	    	this.createForm.controls.fromdate.setValue(jQuery(this.fromdate.nativeElement).val());
      		this.createForm.controls.fromdate.updateValueAndValidity();
	    })
	    /*
	    jQuery(this.todate.nativeElement).datepicker();
	    jQuery(this.todate.nativeElement).on('change', () => {
	    	this.createForm.controls.todate.setValue(jQuery(this.todate.nativeElement).val());
      		this.createForm.controls.todate.updateValueAndValidity();
	    })
	    */
	    
	  }

	search() {
		clearTimeout(this.timer);
		this.timer = setTimeout(() => {
		  this.getListUser(0, this.quantity, this.searchText);
		}, 500)
	}

	setCurrentUser(id){
		this._id = id;
		this.lineChartData[0].data = [0, 0, 0, 0, 0, 0, 0];
		this.lineChartLabels = ['', '' ,'', '', '', '', ''];
	}

	viewtransactions(data){
		this.util.setItemLocalStorage('CURRENT_PARTNER', JSON.stringify(data));
		this.router.navigate(['/detail-user-partner']);
	}

	loadView(){
		if(this.createForm.controls.fromdate.value == ''){
			this.util.handleErrorDate();
		}else{
			var cvtimestamp = new Date(this.createForm.controls.fromdate.value).getTime()
			const url = this.getAmount+ '?partner_id='+ this._id + '&fromdate=' + cvtimestamp + '&nextDayCount=7';
		    this.rest.GET(url, null, null, null, true).subscribe(res => {
		      this.util.hideLoading();
		      if (res && res.data) {
		        this.lineChartData[0].data = res.data.amount;
		        var mydate = new Date();
		    	var newdate = new Date(this.createForm.controls.fromdate.value);
		    	this.lineChartLabels = [];
		    	var i = 1;
		    	for(i = 0; i < 7; i++){
		    		mydate.setDate(newdate.getDate() + i);
		    		this.lineChartLabels.push(mydate.getDate() + '/' + (mydate.getMonth() + 1) + '/' + mydate.getFullYear());
		    	}
		      }
		    }, err => {
		      this.util.hideLoading();
		      this.util.handleError('', err ? err.message : '');
		    })
			
		}
	}


}
