import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { RestService } from '../rest.service';
import * as _ from 'lodash';
import { UtilService } from '../util.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ValidationService } from '../shared/validation';
import { ROLE, ROLE_USER_TEXT, URL, TEXT, MODAL_ID, LINK_CONFIG } from '../shared/constant';
declare var jQuery: any;
import * as moment from 'moment';
import { FileUploader, FileItem, ParsedResponseHeaders } from "ng2-file-upload";
import { concat } from 'rxjs/operators';
import { Observable, generate } from 'rxjs';

@Component({
  selector: 'app-manual-config',
  templateUrl: './manual-config.component.html',
  styleUrls: ['./manual-config.component.css']
})
export class ManualConfigComponent implements OnInit {
  public optionLinkList = LINK_CONFIG;
  public uploader: FileUploader = new FileUploader({ url: URL.UPLOAD, autoUpload: true, removeAfterUpload: true });
  public listFile = {
    header: [],
    footer: []
  };
  public logoHeader: string;
  public logoFooter: string;
  public currentFileType;
  public configForm: FormGroup;
  public config;
  constructor(
    private formBuilder: FormBuilder,
    public validationService: ValidationService,
    private rest: RestService,
    private util: UtilService
  ) {
    this.uploader.onAfterAddingFile = (file) => { file.withCredentials = false; }

    this.uploader.onErrorItem =
      (item, response, status, headers) => this.onErrorItem(item, response, status, headers);

    this.uploader.onSuccessItem =
      (item, response, status, headers) => this.onSuccessItem(item, response, status, headers);
    this.util.showLoading();

    this.configForm = this.formBuilder.group({
      allCategory: ['', [Validators.required]],
      descriptionAllCategory: ['', [Validators.required]],
      hotSale: ['', [Validators.required]],
      Trust: ['', [Validators.required]],
      nearMe: ['', [Validators.required]],
      new: ['', [Validators.required]],
      videoTitle: ['', [Validators.required]],
      videoLink: ['', [Validators.required]],
      titleApp: ['', [Validators.required]],
      descriptionApp: ['', [Validators.required]],
      linkAndroid: ['', [Validators.required]],
      linkIOS: ['', [Validators.required]],
      descriptionFooter: ['', [Validators.required]],
      linkFacebook: ['', [Validators.required]],
      linkGoogle: ['', [Validators.required]],
      linkYoutube: ['', [Validators.required]],
    })


    this.rest.GET(URL.CONFIG, null, null, null, true).subscribe(res => {
      this.config = _.get(res, 'data.items[0]') || {};
      this.configForm.controls.allCategory.setValue(this.config.allCategory);
      this.configForm.controls.descriptionAllCategory.setValue(this.config.descriptionAllCategory);
      this.configForm.controls.hotSale.setValue(this.config.hotSale);
      this.configForm.controls.Trust.setValue(this.config.Trust);
      this.configForm.controls.nearMe.setValue(this.config.nearMe);
      this.configForm.controls.new.setValue(this.config.new);
      this.configForm.controls.videoTitle.setValue(this.config.videoTitle);
      this.configForm.controls.videoLink.setValue(this.config.videoLink);
      this.configForm.controls.titleApp.setValue(this.config.titleApp);
      this.configForm.controls.descriptionApp.setValue(this.config.descriptionApp);
      this.configForm.controls.linkAndroid.setValue(this.config.linkAndroid);
      this.configForm.controls.linkIOS.setValue(this.config.linkIOS);
      this.configForm.controls.descriptionFooter.setValue(this.config.descriptionFooter);
      this.configForm.controls.linkFacebook.setValue(this.config.linkFacebook);
      this.configForm.controls.linkGoogle.setValue(this.config.linkGoogle);
      this.configForm.controls.linkYoutube.setValue(this.config.linkYoutube);
      this.logoHeader = this.util.generateImageUrl(this.config.logoHeader);
      this.logoFooter = this.util.generateImageUrl(this.config.logoFooter);
      this.util.hideLoading();
    },
      (err) => {
        this.util.handleError('', err ? err.message : '');
        this.util.hideLoading();
      })

  }

  ngOnInit() {
  }

  onSuccessItem(item: FileItem, response: string, status: number, headers: ParsedResponseHeaders): any {
    //this gets triggered only once when first file is uploaded
    item.file['url'] = URL.URL_IMAGE + '/' + JSON.parse(item._xhr.response).data[0].filename;
    item.file['base_url'] = JSON.parse(item._xhr.response).data[0].path.replace('public', '');
    this.listFile[this.currentFileType] = [];
    this.listFile[this.currentFileType][0] = item.file;
    (this.currentFileType === "header") && (this.logoHeader = this.listFile.header[0].url);
    (this.currentFileType === "footer") && (this.logoFooter = this.listFile.footer[0].url);
    if (item['uploader'].queue.length === 1) {
      this.util.hideLoading();
    }
  }

  onFileSelected(type) {
    this.currentFileType = type;
    this.util.showLoading();
  }

  onErrorItem(item: FileItem, response: string, status: number, headers: ParsedResponseHeaders): any {
    if (response) {
      let error = JSON.parse(response); //error server response
      if (item['uploader'].queue.length === 1) {
        this.util.hideLoading();
      }
    } else {
      this.util.hideLoading();
      this.util.handleError();
    }
  }

  updateConfig() {
    this.validationService.validateAllFields(this.configForm);
    if(this.configForm.valid) {
      let param = {};
      Object.assign(
        param, 
        this.config, 
        this.configForm.value, 
        {
          logoHeader: _.get(this.listFile, 'header[0].base_url') || this.config.logoHeader, 
          logoFooter: _.get(this.listFile, 'footer[0].base_url') || this.config.logoFooter
        }
      );
      delete param['id'];
      this.util.showLoading();
      this.rest.PUT(URL.CONFIG + '/' + this.config.id, param, null, null, true).subscribe(res => {
        this.util.hideLoading();
        if(res['success']) {
          this.util.handleSuccess(TEXT.SUCCESS_TEXT_UPDATE, TEXT.SUCCESS_TEXT_UPDATE_CONFIG);
        } else {
          this.util.handleError('', res ? res.message : '');
        }
      }, 
      (err) => {
        this.util.hideLoading();
        this.util.handleError('', err ? err.message : '');
      })
    } else {
      this.util.handleError(TEXT.DEFAUL_WARNING, TEXT.ERROR_REQURIRE);
    }
  }

}
