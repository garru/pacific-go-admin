import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManualConfigComponent } from './manual-config.component';

describe('ManualConfigComponent', () => {
  let component: ManualConfigComponent;
  let fixture: ComponentFixture<ManualConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManualConfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManualConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
