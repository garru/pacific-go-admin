import { Component, OnInit } from '@angular/core';
import { RestService } from '../rest.service';
import { UtilService } from '../util.service';
import { URL, KEY_STORAGE } from '../shared/constant';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(
    public rest: RestService,
    public util: UtilService,
    public router: Router
  ) { }

  ngOnInit() {
  }

  logout() {
    this.util.showLoading();
    this.rest.PUT(URL.LOGOUT, null, null, null, true).subscribe(res => {
      this.util.hideLoading();
      if (res.success) {
        this.util.removeItemLocalStorage(KEY_STORAGE.EXPIRY_TIME);
        this.util.removeItemLocalStorage(KEY_STORAGE.LOGIN_TIME);
        this.util.removeItemLocalStorage(KEY_STORAGE.TOKEN);
        this.util.removeItemLocalStorage(KEY_STORAGE.USER_INFOR);
        window.location.pathname = "";
      } else {
        this.util.handleError('', res ? res.message : '');
      }
    }, err => {
      this.util.handleError('', err ? err.message : '');
    })
  }

}
