import { Injectable, ViewChild } from '@angular/core';
import * as CryptoJS from 'crypto-js';
import { TEXT, MODAL_ID, KEY_STORAGE } from './shared/constant';
import * as _ from 'lodash';
declare var jQuery: any;
import * as $ from 'jquery';

@Injectable({
  providedIn: 'root'
})
export class UtilService {
  constructor() { }

  showLoading() {
    const loader = document.querySelector(MODAL_ID.LOADING);
    if (loader) {
      loader.classList.add('show');
    }
  }

  hideLoading() {
    const loader = document.querySelector(MODAL_ID.LOADING);
    if (loader) {
      loader.classList.remove('show');
    }
  }

  convertUrl(url, currentPage, quantity, searchText?) {
    let endUrl;
    if (searchText) {
      endUrl = url + "?search=" + searchText + "&paginate=" + currentPage + "&perPage=" + quantity;
    } else {
      endUrl = url + "?paginate=" + currentPage + "&perPage=" + quantity;
    }
    return endUrl;
  }

  generatePageArr(pageCount, maxPageShow, currentPage) {
    let pageArr = [];
    let pageBalance = Math.floor(maxPageShow / 2);
    if (currentPage + 1 + pageBalance >= pageCount) {
      let minPage = pageCount > maxPageShow ? pageCount - maxPageShow : 1;
      for (let i = minPage; i <= pageCount; i++) {
        let page = {
          index: i,
          class: (i === currentPage + 1) ? 'active' : ''
        };
        pageArr.push(page);
      }
    } else if (currentPage + 1 - pageBalance <= 0) {
      let maxPage = pageCount > maxPageShow ? maxPageShow : pageCount;
      for (let i = 1; i <= maxPage; i++) {
        let page = {
          index: i,
          class: (i === currentPage + 1) ? 'active' : ''
        };
        pageArr.push(page);
      }
    } else {
      for (let i = currentPage + 1 - pageBalance; i <= currentPage + 1 + pageBalance; i++) {
        let page = {
          index: i,
          class: (i === currentPage + 1) ? 'active' : ''
        };
        pageArr.push(page);
      }
    }
    return pageArr;
  }

  // encryption(data: string, key) {
  //   let a = CryptoJS.AES.encrypt("asdfjasodfkjasdlfjasldkfjasierweroik", 'asdfasdf');
  //   let b = CryptoJS.AES.decrypt(this.customStringify(a), 'asdfasdf');
  //   let c = JSON.parse(b.toString());
  //   return a;
  // }

  // decryption(data: string, key) {
  //   let a = CryptoJS.AES.decrypt(JSON.stringify(data), key);
  //   return a;
  // }

  // generatePass() {
  //   return Math.random().toString(36);
  // }

  getUserInfor() {
    let storage = window.localStorage.getItem(KEY_STORAGE.USER_INFOR);
    let userInfor = storage ? JSON.parse(storage) : null;
    return userInfor;
  }

  // checkAuth() {
  //   let userString = this.getUserString();
  //   let userInfo = {};
  //   let isAuth = false;
  //   if (userString && this.isJSONString(userString)) {
  //     const userData = JSON.parse(userString);
  //     if (userData.code && userData.data) {
  //       const infor: any = this.decryption(userData.data, userData.code);
  //       if (infor && this.isJSONString(infor)) {
  //         userInfo = JSON.parse(infor);
  //         const token = userInfo['token'];
  //         isAuth = token ? true : false;
  //       }
  //     }
  //   }
  //   console.log({ isAuth: isAuth, userInfor: userInfo });
  //   return { isAuth: isAuth, userInfor: userInfo };
  // }

  isJSONString(value) {
    try {
      JSON.parse(value);
    } catch (e) {
      return false;
    }
    return true;
  }

  handleError(title?: string, message?: string) {
    const modalError = $(MODAL_ID.ERROR);
    modalError.fadeIn();
    modalError.addClass('show');
    title = title ? title : TEXT.DEFAUL_TITLE;
    message = message ? message : TEXT.DEFAUL_DESC;
    $(MODAL_ID.ERROR_TITLE).text(title);
    $(MODAL_ID.ERORR_DESC).text(message);
  }

  handleDateNull(title?: string, message?: string) {
    const modalError = $(MODAL_ID.ERROR);
    modalError.fadeIn();
    modalError.addClass('show');
    title = title ? title : TEXT.DEFAUL_WARNING;
    message = message ? message : TEXT.ERROR_ISEMPTY_DATA;
    $(MODAL_ID.ERROR_TITLE).text(title);
    $(MODAL_ID.ERORR_DESC).text(message);
  }

  handleErrorDate(title?: string, message?: string) {
    const modalError = $(MODAL_ID.ERROR);
    modalError.fadeIn();
    modalError.addClass('show');
    title = title ? title : TEXT.DEFAUL_ERROR;
    message = message ? message : TEXT.ERROR_ISEMPTY_DATE;
    $(MODAL_ID.ERROR_TITLE).text(title);
    $(MODAL_ID.ERORR_DESC).text(message);
  }

  handleSuccess(title?: string, message?: string) {
    const modalError = $(MODAL_ID.ERROR);
    modalError.fadeIn();
    modalError.addClass('show');
    title = title ? title : TEXT.SUCCESS_TEXT_TITLE;
    message = message ? message : TEXT.SUCCESS_TEXT_DESC;
    $(MODAL_ID.ERROR_TITLE).text(title);
    $(MODAL_ID.ERORR_DESC).text(message);
  }

  closerErrorModal() {
    const modalError = $(MODAL_ID.ERROR);
    modalError.fadeOut();
    setTimeout(() => {
      $(MODAL_ID.ERROR_TITLE).text(TEXT.DEFAUL_TITLE);
      $(MODAL_ID.ERORR_DESC).text(TEXT.DEFAUL_DESC);
    }, 1000)
  }

  // generateToken(data) {
  //   const pass = this.generatePass();
  //   const encryptData = this.encryption(data, pass);
  //   const inforOption = {
  //     code: pass,
  //     data: encryptData
  //   }
  //   window.localStorage.setItem(TEXT.USER_INFOR_STRING, this.customStringify(inforOption));
  //   console.log(this.customStringify(inforOption));
  // }

  customStringify(v) {
    const cache = new Set();
    return JSON.stringify(v, function (key, value) {
      if (typeof value === 'object' && value !== null) {
        if (cache.has(value)) {
          // Circular reference found
          try {
            // If this value does not reference a parent it can be deduped
            return JSON.parse(JSON.stringify(value));
          }
          catch (err) {
            // discard key if value cannot be deduped
            return;
          }
        }
        // Store value in our set
        cache.add(value);
      }
      return value;
    });
  };

  setItemLocalStorage(key, value) {
    window.localStorage.setItem(key, value);
  }

  removeItemLocalStorage(key) {
    window.localStorage.removeItem(key);
  }

  getItemLocalStorage(key) {
    return window.localStorage.getItem(key);
  }

  formatCurrency(money) {
    return money.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
  }

  generateImageUrl(pathUrl) {
    let fullUrl = "";
    if (pathUrl && pathUrl.indexOf("https") === -1) {
      if (pathUrl.indexOf("/admin") !== -1) {
        fullUrl = "https://pacificgo.asia" + pathUrl;
      } else {
        fullUrl = "https://pacificgo.asia/admin" + pathUrl;
      }
    } else {
      fullUrl = pathUrl;
    }
    fullUrl = this.jsonEscape(fullUrl);
    return fullUrl;
  }

  formatUnicode(str) {
    str = str.toLowerCase();
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'| |\"|\&|\#|\[|\]|~|$|_/g, "-");
    str = str.replace(/-+-/g, "-"); //thay thế 2- thành 1-
    str = str.replace(/^\-+|\-+$/g, "");
    return str;
  }

  convertUrlTrans(url, partner_id, fromdate, todate, currentPage, quantity) {
    let endUrl;
    if (todate != 0) {
      endUrl = url + "?partner_id=" + partner_id + "&fromdate=" + fromdate + "&todate=" + todate + "&paginate=" + currentPage + "&perPage=" + quantity;
    } else {
      endUrl = url + "?partner_id=" + partner_id + "&fromdate=" + fromdate + "&paginate=" + currentPage + "&perPage=" + quantity;
    }
    return endUrl;
  }

  convertUrlRole(url, roleId, currentPage, quantity, searchText?) {
    let endUrl;
    if (searchText) {
      endUrl = url + "?search=" + searchText + "&roleId=" + roleId + "&paginate=" + currentPage + "&perPage=" + quantity;
    } else {
      endUrl = url + "?paginate=" + currentPage + "&roleId=" + roleId + "&perPage=" + quantity;
    }
    return endUrl;
  }

  convertUrlHistory(url, currentPage, quantity, searchText?) {
    let endUrl;
    if (searchText) {
      endUrl = url + "?search=" + searchText + "&paginate=" + currentPage + "&perPage=" + quantity;
    } else {
      endUrl = url + "?paginate=" + currentPage + "&perPage=" + quantity;
    }
    return endUrl;
  }

  convertUrlAmountPay(url, userid, month, yearfull?) {
    let endUrl;
    endUrl = url + "?userid=" + userid + "&month=" + month + "&yearfull=" + yearfull;
    return endUrl;
  }

  jsonEscape(string) {
    return  string.replace(/\\n/g, "\\n")
                  .replace(/\\'/g, "\\'")
                  .replace(/\\"/g, '\\"')
                  .replace(/\\&/g, "\\&")
                  .replace(/\\r/g, "\\r")
                  .replace(/\\t/g, "\\t")
                  .replace(/\\b/g, "\\b")
                  .replace(/\\f/g, "\\f")
                  .replace(/\[/g, "")
                  .replace(/\]/g, "");
  }
}
