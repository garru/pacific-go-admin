import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { RestService } from '../rest.service';
import * as _ from 'lodash';
import { UtilService } from '../util.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ValidationService } from '../shared/validation';
import { ROLE, ROLE_USER_TEXT, URL, TEXT, MODAL_ID } from '../shared/constant';
declare var jQuery: any;
import { FileUploader, FileItem, ParsedResponseHeaders } from "ng2-file-upload";

@Component({
  selector: 'app-promo-appove',
  templateUrl: './promo-appove.component.html',
  styleUrls: ['./promo-appove.component.css']
})
export class PromoAppoveComponent implements OnInit {

  @ViewChild('modal') modal: ElementRef;
  @ViewChild('modal2') modal2: ElementRef;
  public quantityOption = [10, 25, 50, 100]; // quantity item show on a page
  public quantityDefault = 10;// quantity default 10
  public quantity = this.quantityDefault;
  public currentPage = 0;
  private getUserUrl = URL.PROMOTIONS_APPROVE;
  public listUser = [];
  public maxPageShow = 5;
  public totalCount;
  public pageArr = [];
  public pageCount;
  public maxEntries;
  public minEntries;
  private currentSort = '_id';
  public createForm: FormGroup;
  public sortOption = {
    id: true,
    name: false,
    email: false,
    price: false
  };
  public promoOnEdit = {};
  private isAsc = true;
  public role = ROLE;
  private roleUser = ROLE_USER_TEXT;
  public pageNumber = [];
  public searchText = "";
  private timer;
  public uploader: FileUploader = new FileUploader({ url: URL.UPLOAD, autoUpload: true, removeAfterUpload: true });
  public listFile = {
    update: [],
    create: []
  };
  public currentFileType;
  public isUpdate = true;
  public currentDeleteCustomerId: number;
  constructor(
    private formBuilder: FormBuilder,
    public validationService: ValidationService,
    private rest: RestService,
    private util: UtilService
  ) {
    this.getListUser(this.currentPage, this.quantity);
    this.uploader.onAfterAddingFile = (file) => { file.withCredentials = false; }
    this.uploader.onErrorItem = (item, response, status, headers) => this.onErrorItem(item, response, status, headers);
    this.uploader.onSuccessItem = (item, response, status, headers) => this.onSuccessItem(item, response, status, headers);
  }

  ngOnInit() {
    this.createForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      price: ['', [Validators.required]],
      quantity: ['', [Validators.required]]
    })
  }

  getListUser(currentPage, quantity, searchText?) {
    this.util.showLoading();
    this.currentPage = currentPage;
    this.quantity = quantity;
    const url = this.util.convertUrl(this.getUserUrl, currentPage, quantity, searchText);
    this.rest.GET(url, null, null, null, true).subscribe(res => {
      this.util.hideLoading();
      if (res && res.data && res.data.items) {
        this.listUser = res.data.items;
        _.forEach(this.listUser, (v, k) => {
          v['featured_images'].forEach((img, idx) => {
            v['featured_images'][idx] = this.util.generateImageUrl(v['featured_images'][idx]);
          })
          v['image'] = this.util.generateImageUrl(v['featured_images'][0]);
        })
        this.pageCount = res.data.pageCount;
        this.pageArr = this.util.generatePageArr(this.pageCount, this.maxPageShow, this.currentPage);
        this.totalCount = res.data.totalCount;
        this.minEntries = this.currentPage * this.quantity + 1;
        this.maxEntries = (this.currentPage + 1) * this.quantity > this.totalCount ? this.totalCount : (this.currentPage + 1) * this.quantity;
        // this.sort(this.currentSort);
      }
    }, err => {
      this.util.hideLoading();
      this.util.handleError('', err ? err.message : '');
    })
  }

  sort(type) {
    if (this.sortOption[type]) {
      this.isAsc = !this.isAsc;
      let option = this.isAsc ? 'asc' : 'desc';
      this.listUser = _.orderBy(this.listUser, [type], [option]);
    } else {
      this.sortOption[this.currentSort] = false;
      this.currentSort = type;
      this.sortOption[type] = true;
      this.listUser = _.orderBy(this.listUser, [type], ['asc']);
    }
  }


  search() {
    clearTimeout(this.timer);
    this.timer = setTimeout(() => {
      this.getListUser(0, this.quantity, this.searchText);
    }, 500)
  }

  fillModalData(user) {
    this.isUpdate = true;
    this.promoOnEdit = user;
    this.createForm.controls.name.setValue(user.name);
    this.createForm.controls.price.setValue(user.price);
    this.createForm.controls.quantity.setValue(user.quantity);
    this.createForm.controls.name.setValidators([]);
    this.createForm.controls.price.setValidators([]);
    this.createForm.controls.quantity.setValidators([]);
    this.listFile.update = [];
  }

  updateUser(id) {
    this.validationService.validateAllFields(this.createForm);
    if(this.createForm.valid) {
      jQuery(this.modal.nativeElement).modal('hide');
      const params = {
        name: this.createForm.controls.name.value,
        price: parseInt(this.createForm.controls.price.value),
        quantity: parseInt(this.createForm.controls.quantity.value)
      }
      params['featured_images'] = [];
      this.listFile.update.forEach(img => {
        params['featured_images'].push(img.base_url);
      })
      this.promoOnEdit['featured_images'].forEach(img => {
        params['featured_images'].push(img);
      })
      this.util.showLoading();
      this.rest.PUT(URL.PROMOTIONS_APPROVE + '/' + id, params, null, null, true).subscribe(res => {
        this.util.hideLoading();
        if (res.success) {
          this.util.handleSuccess(TEXT.SUCCESS_TEXT_UPDATE, TEXT.SUCCESS_TEXT_UPDATE_PROMOTIONS_APPROVE);
          this.getListUser(this.currentPage, this.quantity);
        } else {
          this.util.handleError('', res ? res.message : '');
        }
      }, err => {
        this.util.hideLoading();
        this.util.handleError('', err ? err.message : '');
      })
    } else {
      this.util.handleError(TEXT.DEFAUL_WARNING, TEXT.ERROR_REQURIRE);
    }
  }

  console() {
    console.log(this.createForm.controls.birthday.value);
  }

  onSuccessItem(item: FileItem, response: string, status: number, headers: ParsedResponseHeaders): any {
    //this gets triggered only once when first file is uploaded
    item.file['base_url'] = JSON.parse(item._xhr.response).data[0].path.replace('public', '');
    item.file['url'] = this.util.generateImageUrl(item.file['base_url']);
    // this.listFile[this.currentFileType] = [];
    this.listFile[this.currentFileType].push(item.file);
    if (item['uploader'].queue.length === 1) {
      this.util.hideLoading();
    }
  }

  onFileSelected(type) {
    this.currentFileType = type;
    this.util.showLoading();
  }

  onErrorItem(item: FileItem, response: string, status: number, headers: ParsedResponseHeaders): any {
    if (response) {
      let error = JSON.parse(response); //error server response
      if (item['uploader'].queue.length === 1) {
        this.util.hideLoading();
      }
    } else {
      this.util.hideLoading();
      this.util.handleError();
    }
  }

  createCustomer() {
    this.isUpdate = false;
    this.listFile.create = [];
    this.listFile.update = [];
    this.createForm.controls.name.reset();
    this.createForm.controls.name.setErrors(null);
    this.createForm.controls.price.reset();
    this.createForm.controls.price.setErrors(null);
    this.createForm.controls.quantity.reset();
    this.createForm.controls.quantity.setErrors(null);
    this.createForm.controls.name.setValidators([Validators.required]);
    this.createForm.controls.price.setValidators([Validators.required]);
    this.createForm.controls.quantity.setValidators([Validators.required]);
  }

  createUser() {
    this.validationService.validateAllFields(this.createForm);
    if(this.createForm.valid) {
      jQuery(this.modal.nativeElement).modal('hide');
      const params = {
        name: this.createForm.controls.name.value,
        price: parseInt(this.createForm.controls.price.value),
        quantity: parseInt(this.createForm.controls.quantity.value)
      }
      params['featured_images'] = [];
      this.listFile.create.forEach(img => {
        params['featured_images'].push(img.base_url);
      })
      this.util.showLoading();
      this.rest.POST(URL.PROMOTIONS_APPROVE, params, null, null, true).subscribe(res => {
        this.util.hideLoading();
        if (res.success) {
          this.util.handleSuccess(TEXT.SUCCESS_TEXT_UPDATE, TEXT.SUCCESS_TEXT_CREATE_PROMOTIONS_APPROVE);
          this.getListUser(this.currentPage, this.quantity);
        } else {
          this.util.handleError('', res ? res.message : '');
        }
      }, err => {
        this.util.hideLoading();
        this.util.handleError('', err ? err.message : '');
      })
    } else {
      this.util.handleError(TEXT.DEFAUL_WARNING, TEXT.ERROR_REQURIRE);
    }
   
  }
  
  removeCustomer() {
    this.util.showLoading();
    this.rest.DELETE(URL.PROMOTIONS_APPROVE + '/' + this.currentDeleteCustomerId, null, null, null, true).subscribe(res => {
      this.util.hideLoading();
      if (res.success) {
        this.util.handleSuccess(TEXT.SUCCESS_TEXT_UPDATE, TEXT.SUCCESS_TEXT_DELETE_PROMOTIONS_APPROVE);
        this.getListUser(this.currentPage, this.quantity);
        $('body').removeClass('modal-open');
      } else {
        this.util.handleError('', res ? res.message : '');
      }
    }, err => {
      this.util.hideLoading();
      this.util.handleError('', err ? err.message : '');
    })
  }

  setDeleteCustomer(id) {
    this.currentDeleteCustomerId = id;
  }

  remove(type, index) {
    if(type === 'user') {
      this.promoOnEdit['featured_images'].splice(index, 1);
    } else {
      this.listFile[type].splice(index, 1);
    }
  }

  approve() {
    this.util.showLoading();
    this.rest.PUT(URL.PROMOTIONS_APPROVE + '/' + this.promoOnEdit['promotional_item_id'], null, null, null, true).subscribe(res => {
      this.util.hideLoading();
      if (res.success) {
        this.util.handleSuccess(TEXT.SUCCESS_TEXT_UPDATE, TEXT.SUCCESS_TEXT_DELETE_PROMOTIONS_APPROVE);
        this.getListUser(this.currentPage, this.quantity);
        $('body').removeClass('modal-open');
      } else {
        this.util.handleError('', res ? res.message : '');
      }
    }, err => {
      this.util.hideLoading();
      this.util.handleError('', err ? err.message : '');
    })
  }

  selectPromo(promo) {
    this.promoOnEdit = promo;
  }
}
