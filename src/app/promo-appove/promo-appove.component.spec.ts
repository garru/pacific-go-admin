import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PromoAppoveComponent } from './promo-appove.component';

describe('PromoAppoveComponent', () => {
  let component: PromoAppoveComponent;
  let fixture: ComponentFixture<PromoAppoveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PromoAppoveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PromoAppoveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
