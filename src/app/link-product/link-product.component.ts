import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { RestService } from '../rest.service';
import * as _ from 'lodash';
import { UtilService } from '../util.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ValidationService } from '../shared/validation';
import { ROLE, ROLE_USER_TEXT, URL, TEXT, MODAL_ID } from '../shared/constant';
declare var jQuery: any;
import { FileUploader, FileItem, ParsedResponseHeaders } from "ng2-file-upload";
import * as moment from "moment";

@Component({
  selector: 'app-link-product',
  templateUrl: './link-product.component.html',
  styleUrls: ['./link-product.component.css']
})
export class LinkProductComponent implements OnInit {

  @ViewChild('modal') modal: ElementRef;
  @ViewChild('modal2') modal2: ElementRef;
  public quantityOption = [10, 25, 50, 100]; // quantity item show on a page
  public quantityDefault = 10;// quantity default 10
  public quantity = this.quantityDefault;
  public currentPage = 0;
  private getUserUrl = URL.PROMOTIONS;
  public listUser = [];
  public maxPageShow = 5;
  public totalCount;
  public pageArr = [];
  public pageCount;
  public maxEntries;
  public minEntries;
  private currentSort = '_id';
  public createForm: FormGroup;
  public sortOption = {
    id: true,
    name: false,
    email: false,
    price: false
  };
  public userOnEdit = {};
  private isAsc = true;
  public role = ROLE;
  private roleUser = ROLE_USER_TEXT;
  public pageNumber = [];
  public searchText = "";
  private timer;
  public uploader: FileUploader = new FileUploader({ url: URL.UPLOAD, autoUpload: true, removeAfterUpload: true });
  public listFile = {
    update: [],
    create: []
  };
  public currentFileType;
  public isUpdate = true;
  public currentDeleteCustomerId: number;
  public listAutocompleteUser = [];
  public listAutocompleteProduct = [];
  public user_id: number;
  public username: number;
  public product_id: number;
  public productTitle: number;


  constructor(
    private formBuilder: FormBuilder,
    public validationService: ValidationService,
    private rest: RestService,
    private util: UtilService
  ) {
    this.getListUser(this.currentPage, this.quantity);
  }

  ngOnInit() {
    this.createForm = this.formBuilder.group({
      username: ['', [Validators.required]],
      productTitle: ['', [Validators.required]]
    })
  }

  getListUser(currentPage, quantity, searchText?) {
    this.util.showLoading();
    this.currentPage = currentPage;
    this.quantity = quantity;
    const url = this.util.convertUrl(URL.GET_LINK_PRODUCT, currentPage, quantity, searchText);
    this.rest.GET(url, null, null, null, true).subscribe(res => {
      this.util.hideLoading();
      if (res && res.data && res.data.items) {
        this.listUser = res.data.items;
        this.listUser.forEach(item => {
          item.created = moment.unix(item.created_at).format("YYYY/MM/DD hh:mm:ss");
        })
        this.pageCount = res.data.pageCount;
        this.pageArr = this.util.generatePageArr(this.pageCount, this.maxPageShow, this.currentPage);
        this.totalCount = res.data.totalCount;
        this.minEntries = this.currentPage * this.quantity + 1;
        this.maxEntries = (this.currentPage + 1) * this.quantity > this.totalCount ? this.totalCount : (this.currentPage + 1) * this.quantity;
        // this.sort(this.currentSort);
      }
    }, err => {
      this.util.hideLoading();
      this.util.handleError();
    })
  }

  sort(type) {
    if (this.sortOption[type]) {
      this.isAsc = !this.isAsc;
      let option = this.isAsc ? 'asc' : 'desc';
      this.listUser = _.orderBy(this.listUser, [type], [option]);
    } else {
      this.sortOption[this.currentSort] = false;
      this.currentSort = type;
      this.sortOption[type] = true;
      this.listUser = _.orderBy(this.listUser, [type], ['asc']);
    }
  }


  search() {
    clearTimeout(this.timer);
    this.timer = setTimeout(() => {
      this.getListUser(0, this.quantity, this.searchText);
    }, 500)
  }

  fillModalData(user) {
    this.isUpdate = true;
    this.userOnEdit = user;
    this.username = user.username;
    this.user_id = user.user_id;
    this.productTitle = user.productTitle;
    this.product_id = user.product_id;
    this.createForm.controls.username.setValue(user.username);
    this.createForm.controls.productTitle.setValue(user.productTitle);
    this.createForm.controls.username.setValidators([]);
    this.createForm.controls.productTitle.setValidators([]);
  }

  updateUser(id) {
    this.validationService.validateAllFields(this.createForm);
    if(this.createForm.valid) {
      const params = {
        _id: id,
        user_id: this.user_id,
        product_id: this.product_id
      }
      this.util.showLoading();
      this.rest.PUT(URL.UPDATE_LINK_PRODUCT, params, null, null, true).subscribe(res => {
        this.util.hideLoading();
        jQuery(this.modal.nativeElement).modal('hide');
        if (res.success) {
          this.util.handleSuccess(TEXT.SUCCESS_TEXT_UPDATE, TEXT.SUCCESS_TEXT_UPDATE_LINK_PRODUCT);
          this.getListUser(this.currentPage, this.quantity);
        } else {
          this.util.handleError();
        }
      }, err => {
        this.util.hideLoading();
        this.util.handleError();
      })
    } else {
      this.util.handleError(TEXT.DEFAUL_WARNING, TEXT.ERROR_REQURIRE);
    }
  }

  createCustomer() {
    this.isUpdate = false;
    this.createForm.controls.username.reset();
    this.createForm.controls.username.setErrors(null);
    this.createForm.controls.productTitle.reset();
    this.createForm.controls.productTitle.setErrors(null);
    this.createForm.controls.username.setValidators([Validators.required]);
    this.createForm.controls.productTitle.setValidators([Validators.required]);
  }

  createUser() {
    this.validationService.validateAllFields(this.createForm);
    if(this.createForm.valid) {
      
      const params = {
        user_id: this.user_id,
        product_id: this.product_id
      }
      this.util.showLoading();
      this.rest.POST(URL.GET_LINK_PRODUCT, params, null, null, true).subscribe(res => {
        this.util.hideLoading();
        jQuery(this.modal.nativeElement).modal('hide');
        if (res.success) {
          this.util.handleSuccess(TEXT.SUCCESS_TEXT_UPDATE, TEXT.SUCCESS_TEXT_CREATE_LINK_PRODUCT);
          this.getListUser(this.currentPage, this.quantity);
        } else {
          this.util.handleError();
        }
      }, err => {
        this.util.hideLoading();
        this.util.handleError();
      })
    } else {
      this.util.handleError(TEXT.DEFAUL_WARNING, TEXT.ERROR_REQURIRE);
    }
   
  }
  
  removeCustomer() {
    this.util.showLoading();
    this.rest.DELETE(URL.GET_LINK_PRODUCT + '/' + this.currentDeleteCustomerId, null, null, null, true).subscribe(res => {
      this.util.hideLoading();
      if (res.success) {
        this.util.handleSuccess(TEXT.SUCCESS_TEXT_UPDATE, TEXT.SUCCESS_TEXT_DELETE_LINK_PRODUCT);
        this.getListUser(this.currentPage, this.quantity);
        $('body').removeClass('modal-open');
      } else {
        this.util.handleError();
      }
    }, err => {
      this.util.hideLoading();
      this.util.handleError();
    })
  }

  setDeleteCustomer(id) {
    this.currentDeleteCustomerId = id;
  }

  getUser() {
    clearTimeout(this.timer);
    this.timer = setTimeout(() => {
      if (this.createForm.controls.username.valid && 
          this.createForm.controls.username.value.length > 2) {
        let name = this.createForm.controls.username.value;
        let url = this.util.convertUrl(URL.GET_USER_LIST, 0, 10, name); // get 10 user
        this.util.showLoading();
        this.rest.GET(url, null, null, null, true).subscribe(res => {
          this.util.hideLoading();
          if (res && res.data && res.data.items) {
            this.listAutocompleteUser = res.data.items
          } else {
            this.listAutocompleteUser = [];
          }
        }, () => {
          this.util.hideLoading();
          this.listAutocompleteUser = [];
        })
      }
    }, 200)
  }
  selectUser(name, id) {
    this.createForm.controls.username.setValue(name);
    this.user_id = id;
    this.username = name;
    this.listAutocompleteUser = [];
  }

  resetListAutocompleteUser() {
    setTimeout(() => {
      this.listAutocompleteUser = [];
      this.createForm.controls.username.setValue(this.username);
    }, 500)
  }

  getProduct() {
    clearTimeout(this.timer);
    this.timer = setTimeout(() => {
      if (this.createForm.controls.productTitle.valid && 
          this.createForm.controls.productTitle.value.length > 2) {
        let name = this.createForm.controls.productTitle.value;
        let url = this.util.convertUrl(URL.PRODUCT_LIST, 0, 10, name); // get 10 user
        this.util.showLoading();
        this.rest.GET(url, null, null, null, true).subscribe(res => {
          this.util.hideLoading();
          if (res && res.data && res.data.items) {
            this.listAutocompleteProduct = res.data.items;
          } else {
            this.listAutocompleteProduct = [];
          }
        }, () => {
          this.util.hideLoading();
          this.listAutocompleteProduct = [];
        })
      }
    }, 200)
  }
  selectProduct(name, id) {
    this.createForm.controls.productTitle.setValue(name);
    this.product_id = id;
    this.productTitle = name;
    this.listAutocompleteProduct = [];
  }

  resetListAutocompleteProduct() {
    setTimeout(() => {
      this.listAutocompleteProduct = [];
      this.createForm.controls.productTitle.setValue(this.productTitle);
    }, 500)
  }

}
