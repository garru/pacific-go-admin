import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { RestService } from '../rest.service';
import * as _ from 'lodash';
import { UtilService } from '../util.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ValidationService } from '../shared/validation';
import { ROLE, ROLE_USER_TEXT, URL, TEXT, MODAL_ID } from '../shared/constant';
declare var jQuery: any;
import * as moment from 'moment';
import { FileUploader, FileItem, ParsedResponseHeaders } from "ng2-file-upload";

@Component({
  selector: 'app-customer-manager',
  templateUrl: './customer-manager.component.html',
  styleUrls: ['./customer-manager.component.css']
})

export class CustomerManagerComponent implements OnInit {
  @ViewChild('datepicker') date: ElementRef;
  @ViewChild('modal') modal: ElementRef;
  @ViewChild('modal2') modal2: ElementRef;
  public quantityOption = [10, 25, 50, 100]; // quantity item show on a page
  public quantityDefault = 10;// quantity default 10
  public quantity = this.quantityDefault;
  public currentPage = 0;
  private getUserUrl = URL.GET_CUSTOMER_LIST;
  public listUser = [];
  public maxPageShow = 5;
  public totalCount;
  public pageArr = [];
  public pageCount;
  public maxEntries;
  public minEntries;
  private currentSort = '_id';
  public createForm: FormGroup;
  public sortOption = {
    id: true,
    customerName: false,
    email: false,
    customerCompany: false
  };
  public userOnEdit = {};
  private isAsc = true;
  public role = ROLE;
  private roleUser = ROLE_USER_TEXT;
  public pageNumber = [];
  public searchText = "";
  private timer;
  public uploader: FileUploader = new FileUploader({ url: URL.UPLOAD, autoUpload: true, removeAfterUpload: true });
  public listFile = {
    update: [],
    create: []
  };
  public currentFileType;
  public isUpdate = true;
  public currentDeleteCustomerId: number;
  constructor(
    private formBuilder: FormBuilder,
    public validationService: ValidationService,
    private rest: RestService,
    private util: UtilService
  ) {
    this.getListUser(this.currentPage, this.quantity);
    this.uploader.onAfterAddingFile = (file) => { file.withCredentials = false; }
    this.uploader.onErrorItem = (item, response, status, headers) => this.onErrorItem(item, response, status, headers);
    this.uploader.onSuccessItem = (item, response, status, headers) => this.onSuccessItem(item, response, status, headers);
  }

  ngOnInit() {
    this.createForm = this.formBuilder.group({
      customerName: ['', [Validators.required]],
      customerCompany: ['', [Validators.required]],
      customerImage: ['', [Validators.required]],
      customerDescription: ['', [Validators.required]],
      status: [null]
    })
  }

  getListUser(currentPage, quantity, searchText?) {
    this.util.showLoading();
    this.currentPage = currentPage;
    this.quantity = quantity;
    const url = this.util.convertUrl(this.getUserUrl, currentPage, quantity, searchText);
    this.rest.GET(url, null, null, null, true).subscribe(res => {
      this.util.hideLoading();
      if (res && res.data) {
        this.listUser = res.data;
        _.forEach(this.listUser, (v, k) => {
          v['customerImage'] = this.util.generateImageUrl(v['customerImage']);
        })
        this.pageCount = res.data.pageCount;
        this.pageArr = this.util.generatePageArr(this.pageCount, this.maxPageShow, this.currentPage);
        this.totalCount = res.data.totalCount;
        this.minEntries = this.currentPage * this.quantity + 1;
        this.maxEntries = (this.currentPage + 1) * this.quantity > this.totalCount ? this.totalCount : (this.currentPage + 1) * this.quantity;
        // this.sort(this.currentSort);
      }
    }, err => {
      this.util.hideLoading();
      this.util.handleError('', err ? err.message : '');
    })
  }

  sort(type) {
    if (this.sortOption[type]) {
      this.isAsc = !this.isAsc;
      let option = this.isAsc ? 'asc' : 'desc';
      this.listUser = _.orderBy(this.listUser, [type], [option]);
    } else {
      this.sortOption[this.currentSort] = false;
      this.currentSort = type;
      this.sortOption[type] = true;
      this.listUser = _.orderBy(this.listUser, [type], ['asc']);
    }
  }


  search() {
    clearTimeout(this.timer);
    this.timer = setTimeout(() => {
      this.getListUser(0, this.quantity, this.searchText);
    }, 500)
  }

  fillModalData(user) {
    this.isUpdate = true;
    this.userOnEdit = user;
    this.createForm.controls.customerName.setValue(user.customerName);
    this.createForm.controls.customerCompany.setValue(user.customerCompany);
    this.createForm.controls.customerImage.setValue(user.customerImage);
    this.createForm.controls.customerDescription.setValue(user.customerDescription);
    this.createForm.controls.status.setValue(user.status ? true : false);
    this.createForm.controls.customerName.setValidators([]);
    this.createForm.controls.customerCompany.setValidators([]);
    this.createForm.controls.customerImage.setValidators([]);
    this.createForm.controls.customerDescription.setValidators([]);
    this.listFile.update = [];
  }

  updateUser(id) {
    this.validationService.validateAllFields(this.createForm);
    if(this.createForm.valid) {
      jQuery(this.modal.nativeElement).modal('hide');
      const params = {
        customerName: this.createForm.controls.customerName.value,
        customerCompany: this.createForm.controls.customerCompany.value,
        customerDescription: this.createForm.controls.customerDescription.value
      }
      params['customerImage'] = this.listFile.update[0] ? this.listFile.update[0].base_url : this.userOnEdit['customerImage'];
      this.util.showLoading();
      this.rest.PUT(URL.GET_CUSTOMER_LIST + '/' + id, params, null, null, true).subscribe(res => {
        this.util.hideLoading();
        if (res.success) {
          this.util.handleSuccess(TEXT.SUCCESS_TEXT_UPDATE, TEXT.SUCCESS_TEXT_UPDATE_CUSTOMER);
          this.getListUser(this.currentPage, this.quantity);
        } else {
          this.util.handleError('', res ? res.message : '');
        }
      }, err => {
        this.util.hideLoading();
        this.util.handleError('', err ? err.message : '');
      })
    } else {
      this.util.handleError(TEXT.DEFAUL_WARNING, TEXT.ERROR_REQURIRE);
    }
  }

  console() {
    console.log(this.createForm.controls.birthday.value);
  }

  onSuccessItem(item: FileItem, response: string, status: number, headers: ParsedResponseHeaders): any {
    //this gets triggered only once when first file is uploaded
    item.file['url'] = URL.URL_IMAGE + '/' + JSON.parse(item._xhr.response).data[0].filename;
    item.file['base_url'] = JSON.parse(item._xhr.response).data[0].path.replace('public', '');
    this.listFile[this.currentFileType] = [];
    this.listFile[this.currentFileType].push(item.file);
    if (item['uploader'].queue.length === 1) {
      this.util.hideLoading();
    }
  }

  onFileSelected(type) {
    this.currentFileType = type;
    this.util.showLoading();
  }

  onErrorItem(item: FileItem, response: string, status: number, headers: ParsedResponseHeaders): any {
    if (response) {
      let error = JSON.parse(response); //error server response
      if (item['uploader'].queue.length === 1) {
        this.util.hideLoading();
      }
    } else {
      this.util.hideLoading();
      this.util.handleError();
    }
  }

  createCustomer() {
    this.isUpdate = false;
    this.listFile.create = [];
    this.listFile.update = [];
    this.createForm.controls.customerName.reset();
    this.createForm.controls.customerName.setErrors(null);
    this.createForm.controls.customerCompany.reset();
    this.createForm.controls.customerCompany.setErrors(null);
    this.createForm.controls.customerImage.reset();
    this.createForm.controls.customerImage.setErrors(null);
    this.createForm.controls.customerDescription.reset();
    this.createForm.controls.customerDescription.setErrors(null);
    this.createForm.controls.customerName.setValidators([Validators.required]);
    this.createForm.controls.customerCompany.setValidators([Validators.required]);
    this.createForm.controls.customerImage.setValidators([Validators.required]);
    this.createForm.controls.customerDescription.setValidators([Validators.required]);
  }

  createUser(id) {
    this.validationService.validateAllFields(this.createForm);
    if(this.createForm.valid) {
      jQuery(this.modal.nativeElement).modal('hide');
      const params = {
        customerName: this.createForm.controls.customerName.value,
        customerCompany: this.createForm.controls.customerCompany.value,
        customerDescription: this.createForm.controls.customerDescription.value,
        status: 1
      }
      this.listFile.create[0] && (params['customerImage'] = this.listFile.create[0].base_url);
      this.util.showLoading();
      this.rest.POST(URL.GET_CUSTOMER_LIST, params, null, null, true).subscribe(res => {
        this.util.hideLoading();
        if (res.success) {
          this.util.handleSuccess(TEXT.SUCCESS_TEXT_UPDATE, TEXT.SUCCESS_TEXT_CREATE_CUSTOMER);
          this.getListUser(this.currentPage, this.quantity);
        } else {
          this.util.handleError('', res ? res.message : '');
        }
      }, err => {
        this.util.hideLoading();
        this.util.handleError('', err ? err.message : '');
      })
    } else {
      this.util.handleError(TEXT.DEFAUL_WARNING, TEXT.ERROR_REQURIRE);
    }
  }
  
  removeCustomer() {
    this.util.showLoading();
    this.rest.DELETE(URL.REMOVE_CUSTOMER + this.currentDeleteCustomerId, null, null, null, true).subscribe(res => {
      this.util.hideLoading();
      if (res.success) {
        this.util.handleSuccess(TEXT.SUCCESS_TEXT_UPDATE, TEXT.SUCCESS_TEXT_DELETE_CUSTOMER);
        this.getListUser(this.currentPage, this.quantity);
        $('body').removeClass('modal-open');
      } else {
        this.util.handleError('', res ? res.message : '');
      }
    }, err => {
      this.util.hideLoading();
      this.util.handleError('', err ? err.message : '');
    })
  }

  setDeleteCustomer(id) {
    this.currentDeleteCustomerId = id;
  }
}
