import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { AppComponent } from './app.component';
import { UserManagerComponent } from './user-manager/user-manager.component';
import { RouterModule, Routes } from '@angular/router';
import { SideBarComponent } from './side-bar/side-bar.component';
import { HeaderComponent } from './header/header.component';
import { UtilService } from './util.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RestService } from './rest.service';
import { CreateUserComponent } from './create-user/create-user.component';
import { LoginComponent } from './login/login.component';
import { ValidationControlMessages } from './shared/validation.control-message';
import { ValidationService } from './shared/validation';
import { ProductManagerComponent } from './product-manager/product-manager.component';
import { CreateProductComponent } from './create-product/create-product.component';
// import { NgxEditorModule } from 'ngx-editor';
import { CKEditorModule } from 'ng2-ckeditor';
import { FileUploadModule } from 'ng2-file-upload';
import { AgmCoreModule } from '@agm/core';
import { GOOGLE_MAP_API_KEY } from './shared/constant';
import { CustomerManagerComponent } from './customer-manager/customer-manager.component';
import { TransactionPartnerComponent } from './transaction-partner/transaction-partner.component';
import { TransactionPacificComponent } from './transaction-pacific/transaction-pacific.component';
import { UpdateProductComponent } from './update-product/update-product.component';
import { CategoriesManagerComponent } from './categories-manager/categories-manager.component';
import { ConfigComponent } from './config/config.component';
import { PartnerManagerComponent } from './partner-manager/partner-manager.component';
import { AdsManagerComponent } from './ads-manager/ads-manager.component';
import { ManualConfigComponent } from './manual-config/manual-config.component';
import { UserPartnerComponent } from './user-partner/user-partner.component';
import { DetailUserPartnerComponent } from './detail-user-partner/detail-user-partner.component';

import { ChartsModule } from 'ng2-charts/ng2-charts';
import { PromoManagerComponent } from './promo-manager/promo-manager.component';
import { PromoAppoveComponent } from './promo-appove/promo-appove.component';
import { ProductApproveComponent } from './product-approve/product-approve.component';
import { LinkProductComponent } from './link-product/link-product.component';
import { PaymentComponent } from './payment/payment.component';
import { TransactionCommitComponent } from './transaction-commit/transaction-commit.component';



const routes: Routes = [
  {
    path: '',
    component: UserManagerComponent
  },
  {
    path: 'user-manager',
    component: UserManagerComponent
  },
  {
    path: 'user-partner',
    component: UserPartnerComponent,
    data: { title: "Danh sách đối tác", roleId: '2' }
  },
  {
    path: 'user-member',
    component: UserPartnerComponent,
    data: { title: "Danh sách thành viên", roleId: '4' }
  },
  {
    path: 'user-customer',
    component: UserPartnerComponent,
    data: { title: "Danh sách khách hàng", roleId: '3' }
  },
  {
    path: 'detail-user-partner',
    component: DetailUserPartnerComponent
  },
  {
    path: 'new-user',
    component: CreateUserComponent
  },
  {
    path: 'payment',
    component: PaymentComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'product-manager',
    component: ProductManagerComponent
  },
  {
    path: 'create-product',
    component: CreateProductComponent
  },
  {
    path: 'customer-manager',
    component: CustomerManagerComponent
  },
  {
    path: 'transaction-partner',
    component: TransactionPartnerComponent
  },
  {
    path: 'transaction-commit',
    component: TransactionCommitComponent
  },
  {
    path: 'transaction-pacific',
    component: TransactionPacificComponent
  },
  {
    path: 'edit-product',
    component: UpdateProductComponent
  },
  {
    path: 'categories',
    component: CategoriesManagerComponent
  },
  {
    path: 'partner',
    component: PartnerManagerComponent
  },
  {
    path: 'ads-manager',
    component: AdsManagerComponent
  },
  {
    path: 'manual-config',
    component: ManualConfigComponent
  },
  {
    path: 'promo-manager',
    component: PromoManagerComponent
  },
  {
    path: 'promo-approve',
    component: PromoAppoveComponent
  },
  {
    path: 'product-approve',
    component: ProductApproveComponent
  },
  {
    path: 'link-product',
    component: LinkProductComponent
  },
]
@NgModule({
  declarations: [
    AppComponent,
    UserManagerComponent,
    SideBarComponent,
    HeaderComponent,
    CreateUserComponent,
    LoginComponent,
    ValidationControlMessages,
    ProductManagerComponent,
    CreateProductComponent,
    CustomerManagerComponent,
    TransactionPartnerComponent,
    TransactionPacificComponent,
    UpdateProductComponent,
    CategoriesManagerComponent,
    ConfigComponent,
    PartnerManagerComponent,
    AdsManagerComponent,
    ManualConfigComponent,
    UserPartnerComponent,
    DetailUserPartnerComponent,
    PromoManagerComponent,
    PromoAppoveComponent,
    ProductApproveComponent,
    LinkProductComponent,
    PaymentComponent,
    TransactionCommitComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    // NgxEditorModule,
    CKEditorModule,
    FileUploadModule,
    AgmCoreModule.forRoot({
      apiKey: GOOGLE_MAP_API_KEY,
      libraries: ["places"]
    }),
    ChartsModule,
  ],
  exports: [
    RouterModule
  ],
  providers: [
    UtilService,
    RestService,
    ValidationControlMessages,
    ValidationService
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
