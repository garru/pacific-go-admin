import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { RestService } from '../rest.service';
import * as _ from 'lodash';
import { UtilService } from '../util.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ValidationService } from '../shared/validation';
import { ROLE, ROLE_USER_TEXT, URL, TEXT, MODAL_ID } from '../shared/constant';
declare var jQuery: any;
import * as moment from 'moment';
import { FileUploader, FileItem, ParsedResponseHeaders } from "ng2-file-upload";
import { concat } from 'rxjs/operators';
import { Observable, generate } from 'rxjs';
@Component({
  selector: 'app-categories-manager',
  templateUrl: './categories-manager.component.html',
  styleUrls: ['./categories-manager.component.css']
})
export class CategoriesManagerComponent implements OnInit {
  @ViewChild('datepicker') date: ElementRef;
  @ViewChild('modal') modal: ElementRef;
  @ViewChild('modal2') modal2: ElementRef;
  public quantityOption = [10, 25, 50, 100]; // quantity item show on a page
  public quantityDefault = 10;// quantity default 10
  public quantity = this.quantityDefault;
  public currentPage = 0;
  private getUserUrl = URL.GET_CUSTOMER_LIST;
  private listUser_total = [];
  public listUser = [];
  public maxPageShow = 5;
  public totalCount;
  public pageArr = [];
  public pageCount;
  public maxEntries;
  public minEntries;
  private currentSort = '_id';
  public createForm: FormGroup;
  public sortOption = {
    id: true,
    customerName: false,
    email: false,
    customerCompany: false
  };
  public userOnEdit = {};
  private isAsc = true;
  public role = ROLE;
  private roleUser = ROLE_USER_TEXT;
  public pageNumber = [];
  public searchText = "";
  private timer;
  public uploader: FileUploader = new FileUploader({ url: URL.UPLOAD, autoUpload: true, removeAfterUpload: true });
  public listFile = {
    update: [],
    create: []
  };
  public currentFileType;
  public isUpdate = true;
  public currentDeleteCustomerId: number;
  public categoriesList = [];
  constructor(
    private formBuilder: FormBuilder,
    public validationService: ValidationService,
    private rest: RestService,
    private util: UtilService
  ) {
    this.getListUser(this.currentPage, this.quantity);
    this.uploader.onAfterAddingFile = (file) => { file.withCredentials = false; }
    this.uploader.onErrorItem = (item, response, status, headers) => this.onErrorItem(item, response, status, headers);
    this.uploader.onSuccessItem = (item, response, status, headers) => this.onSuccessItem(item, response, status, headers);
  }

  ngOnInit() {
    this.createForm = this.formBuilder.group({
      customerName: ['', [Validators.required]],
      customerCompany: ['', [Validators.required]],
      customerImage: ['', [Validators.required]],
      customerDescription: ['', [Validators.required]],
      parentId: ['']
    })
  }

  getListUser(currentPage, quantity, searchText?) {
    this.util.showLoading();
    this.currentPage = currentPage;
    this.quantity = quantity;
    const url = this.util.convertUrl(this.getUserUrl, currentPage, quantity, searchText);
    this.rest.GET(URL.GET_CATEGORIES, null, '', false, true).subscribe(res => {
      this.util.hideLoading();
      if (res && res.data) {
        // let a = { transaction_id: "b10f9920-110c-11e9-9562-57f102529ec5", discount: 258000, username: "admin" };
        // for (let i = 0; i < 50; i++) {
        //   this.data_transaction_pacific_total.push(a);
        // }
        this.categoriesList = res.data.items;
        this.listUser_total = res.data.items;
        let listGetSub = [];
        listGetSub = this.listUser_total;
        let length = this.listUser_total.length;
        this.listUser_total.forEach((item, idx) => {
          this.getSubCategories(item.id).subscribe(res => {
            if (res.data && res.data.items) {
              listGetSub = listGetSub.concat(res.data.items);
            }
            if (idx === length - 1) {
              this.listUser_total = listGetSub;
              this.util.hideLoading();
              this.generateListCategories(this.listUser_total);
              // this.sort(this.currentSort);
              this.totalCount = this.listUser_total.length;
              this.changePage(this.currentPage, this.quantity);
            }
          }, err => {
            this.util.handleError('', err ? err.message : '');
            if (idx === length - 1) {
              this.util.hideLoading();
            }
          })
        })
      } else {
        this.util.handleError('', res ? res.message : '');
      }
    }, err => {
      this.util.hideLoading();
      this.util.handleError('', err ? err.message : '');
    })
  }

  sort(type) {
    if (this.sortOption[type]) {
      this.isAsc = !this.isAsc;
      let option = this.isAsc ? 'asc' : 'desc';
      this.listUser = _.orderBy(this.listUser, [type], [option]);
    } else {
      this.sortOption[this.currentSort] = false;
      this.currentSort = type;
      this.sortOption[type] = true;
      this.listUser = _.orderBy(this.listUser, [type], ['asc']);
    }
  }


  search() {
    clearTimeout(this.timer);
    this.timer = setTimeout(() => {
      this.getListUser(0, this.quantity, this.searchText);
    }, 500)
  }

  fillModalData(user) {
    this.isUpdate = true;
    this.userOnEdit = user;
    this.createForm.controls.customerName.setValue(user.name, {emitEvent: false});
    this.createForm.controls.customerCompany.setValue(user.slug, {emitEvent: false});
    this.createForm.controls.customerImage.setValue(user.customerImage, {emitEvent: false});
    this.createForm.controls.customerDescription.setValue(user.description, {emitEvent: false});
    this.createForm.controls.parentId.setValue(user.category_id || user.parentId, {emitEvent: false});
    this.createForm.controls.customerName.setValidators([]);
    this.createForm.controls.customerCompany.setValidators([]);
    this.createForm.controls.customerImage.setValidators([]);
    this.createForm.controls.customerDescription.setValidators([]);
    this.listFile.update[0] = {};
    this.listFile.update[0].url = user.fullUrl;
  }

  updateUser(id) {
    this.validationService.validateAllFields(this.createForm);
    if (this.createForm.valid) {
      jQuery(this.modal.nativeElement).modal('hide');
      const params = {
        name: this.createForm.controls.customerName.value,
        slug: this.createForm.controls.customerCompany.value || "",
        description: this.createForm.controls.customerDescription.value || ""
      }
      // this.listFile.update[0] && (params['customerImage'] = this.listFile.update[0].url);
      !!this.createForm.controls.parentId.value && this.listFile.update[0] && (params['categoryThumb'] = this.listFile.update[0].url);
      !this.createForm.controls.parentId.value && this.listFile.update[0] && (params['featured_image'] = this.listFile.update[0].url);
      !this.createForm.controls.parentId.value  && (params['parentId'] = parseInt(this.createForm.controls.parentId.value) || 0);
      this.createForm.controls.parentId.value  && (params['category_id'] = parseInt(this.createForm.controls.parentId.value) || 0);
      this.util.showLoading();
      let url = !!this.createForm.controls.parentId.value ? URL.ALL_CATEGORIES : URL.SUB_CATEGORY;
      this.rest.PUT(url + '/' + id, params, null, null, true).subscribe(res => {
        this.util.hideLoading();
        if (res.success) {
          this.util.handleSuccess(TEXT.SUCCESS_TEXT_UPDATE, TEXT.SUCCESS_TEXT_UPDATE_CATE);
          this.getListUser(this.currentPage, this.quantity);
        } else {
          this.util.handleError('', res ? res.message : '');
        }
      }, err => {
        this.util.hideLoading();
        this.util.handleError('', err ? err.message : '');
      })
    } else {
      this.util.handleError(TEXT.DEFAUL_WARNING, TEXT.ERROR_REQURIRE);
    }
  }

  onSuccessItem(item: FileItem, response: string, status: number, headers: ParsedResponseHeaders): any {
    //this gets triggered only once when first file is uploaded
    item.file['url'] = URL.URL_IMAGE + '/' + JSON.parse(item._xhr.response).data[0].filename;
    item.file['base_url'] = JSON.parse(item._xhr.response).data[0].path.replace('public', '');
    this.listFile[this.currentFileType] = [];
    this.listFile[this.currentFileType][0] = item.file;
    if (item['uploader'].queue.length === 1) {
      this.util.hideLoading();
    }
  }

  onFileSelected(type) {
    this.currentFileType = type;
    this.util.showLoading();
  }

  onErrorItem(item: FileItem, response: string, status: number, headers: ParsedResponseHeaders): any {
    if (response) {
      let error = JSON.parse(response); //error server response
      if (item['uploader'].queue.length === 1) {
        this.util.hideLoading();
      }
    } else {
      this.util.hideLoading();
      this.util.handleError();
    }
  }

  createCustomer() {
    this.isUpdate = false;
    this.listFile.create = [];
    this.listFile.update = [];
    this.createForm.controls.customerName.reset();
    this.createForm.controls.parentId.reset();
    this.createForm.controls.customerName.setErrors(null);
    this.createForm.controls.customerCompany.reset();
    this.createForm.controls.customerCompany.setErrors(null);
    this.createForm.controls.customerImage.reset();
    this.createForm.controls.customerImage.setErrors(null);
    this.createForm.controls.customerDescription.reset();
    this.createForm.controls.customerDescription.setErrors(null);
    this.createForm.controls.customerName.setValidators([Validators.required]);
    this.createForm.controls.customerCompany.setValidators([Validators.required]);
    this.createForm.controls.customerImage.setValidators([Validators.required]);
    this.createForm.controls.customerDescription.setValidators([Validators.required]);
  }

  createUser(id) {
    this.validationService.validateAllFields(this.createForm);
    if (this.createForm.valid) {
      jQuery(this.modal.nativeElement).modal('hide');
      const params = {
        name: this.createForm.controls.customerName.value,
        slug: this.createForm.controls.customerCompany.value || "",
        description: this.createForm.controls.customerDescription.value || "",
        status: 0
      }
      !this.createForm.controls.parentId.value && this.listFile.create[0] && (params['categoryThumb'] = this.listFile.create[0].url);
      this.createForm.controls.parentId.value && this.listFile.create[0] && (params['featured_image'] = this.listFile.create[0].url);
      !this.createForm.controls.parentId.value  && (params['parentId'] = parseInt(this.createForm.controls.parentId.value) || 0);
      this.createForm.controls.parentId.value  && (params['category_id'] = parseInt(this.createForm.controls.parentId.value) || 0);
      this.util.showLoading();
      let url = !this.createForm.controls.parentId.value ? URL.ALL_CATEGORIES : URL.SUB_CATEGORY;
      this.rest.POST(url, params, null, null, true).subscribe(res => {
        this.util.hideLoading();
        if (res.success) {
          this.util.handleSuccess(TEXT.SUCCESS_TEXT_UPDATE, TEXT.SUCCESS_TEXT_DELETE_CATE);
          this.getListUser(this.currentPage, this.quantity);
        } else {
          this.util.handleError('', res ? res.message : '');
        }
      }, err => {
        this.util.hideLoading();
        this.util.handleError('', err ? err.message : '');
      })
    } else {
      this.util.handleError(TEXT.DEFAUL_WARNING, TEXT.ERROR_REQURIRE);
    }
  }

  removeCustomer() {
    this.util.showLoading();
    let url = (this.currentDeleteCustomerId['parentId'] || this.currentDeleteCustomerId['category_id']) ? URL.ALL_CATEGORIES : URL.SUB_CATEGORY;
    this.rest.DELETE(URL.ALL_CATEGORIES + '/' + this.currentDeleteCustomerId['id'], null, null, null, true).subscribe(res => {
      this.util.hideLoading();
      if (res.success) {
        this.util.handleSuccess(TEXT.SUCCESS_TEXT_UPDATE, TEXT.SUCCESS_TEXT_DELETE_CUSTOMER);
        this.getListUser(this.currentPage, this.quantity);
        $('body').removeClass('modal-open');
      } else {
        this.util.handleError('', res ? res.message : '');
      }
    }, err => {
      this.util.hideLoading();
      this.util.handleError('', err ? err.message : '');
    })
  }

  setDeleteCustomer(id) {
    this.currentDeleteCustomerId = id;
  }

  changePage(idx, quantity) {
    this.currentPage = idx;
    this.quantity = quantity;
    this.pageCount = Math.ceil(this.totalCount / this.quantity);
    this.pageArr = this.util.generatePageArr(this.pageCount, this.maxPageShow, this.currentPage);
    this.minEntries = this.currentPage * this.quantity + 1;
    this.maxEntries = (this.currentPage + 1) * this.quantity > this.totalCount ? this.totalCount : (this.currentPage + 1) * this.quantity;
    this.listUser = this.listUser_total.filter((item, index) => {
      return (index >= idx * quantity) && (index < (idx + 1) * quantity);
    });
  }

  getSubCategories(id): Observable<any> {
    return this.rest.GET(URL.GET_SUB_CATEGORIES + id, null, null, null, true);
  }

  generateListCategories(list) {
    list.forEach((item, idx) => {
      if (item.featured_image || item.categoryThumb) {
        list[idx].fullUrl = this.util.generateImageUrl(list[idx].featured_image || list[idx].categoryThumb);
      }
      if (item.category_id || item.parentId) {
        let temp = list.find(cate => cate.id === item.category_id || cate.id === item.parentId).name;
        list[idx].father = temp;
      }
    })
  }
}
