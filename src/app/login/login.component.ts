import {Component, OnInit }from '@angular/core'; 
declare var jQuery:any; 
import {ValidationService }from '../shared/validation'; 
import {FormBuilder, FormGroup, Validators, FormControl }from '@angular/forms'; 
import {RestService }from '../rest.service'; 
import {UtilService }from '../util.service'; 
import {Router }from '@angular/router'; 
import {URL, TEXT, KEY_STORAGE }from '../shared/constant'; 
import * as moment from 'moment'; 

@Component( {
  selector:'app-login', 
  templateUrl:'./login.component.html', 
  styleUrls:['./login.component.css']
})
export class LoginComponent implements OnInit {
  public loginForm:FormGroup; 
  constructor(
    private formBuilder:FormBuilder, 
    public validationService:ValidationService, 
    private rest:RestService, 
    private util:UtilService, 
    private router:Router
  ) {}

  ngOnInit() {
    this.loginForm = this.formBuilder.group( {
      username:['', [Validators.required]], 
      password:['', [Validators.required]]
    })
  }

  doLogin() {
    if (this.loginForm.valid) {
      const param =  {
        username:this.loginForm.controls.username.value, 
        password:this.loginForm.controls.password.value
      }
      this.util.showLoading(); 
      this.rest.POST(URL.LOGIN, param).subscribe(res =>  {
        if (res.success && res.data && res.data.User && res.data.User.userRoleId === 1) {
          this.util.setItemLocalStorage(KEY_STORAGE.LOGIN_TIME, moment.utc().toISOString()); 
          this.util.setItemLocalStorage(KEY_STORAGE.EXPIRY_TIME, res.data.expiredTime); 
          this.util.setItemLocalStorage(KEY_STORAGE.USER_INFOR, JSON.stringify(res.data.User)); 
          this.util.setItemLocalStorage(KEY_STORAGE.TOKEN, res.data.token); 
          window.location.pathname = ""; 
        }else {
          this.util.hideLoading(); 
          this.util.handleError(TEXT.ERROR_TEXT_LOGIN); 
        }
      }, err =>  {
        this.util.hideLoading(); 
        this.util.handleError(); 
      })
    } else {
      this.util.handleError(TEXT.DEFAUL_WARNING, TEXT.ERROR_REQURIRE);
    }
  }

}
