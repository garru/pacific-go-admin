import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { RestService } from '../rest.service';
import * as _ from 'lodash';
import { UtilService } from '../util.service';
import { ROLE, ROLE_USER_TEXT, URL, TEXT, MODAL_ID } from '../shared/constant';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-product-approve',
  templateUrl: './product-approve.component.html',
  styleUrls: ['./product-approve.component.css']
})
export class ProductApproveComponent implements OnInit {

  public quantityOption = [10, 25, 50, 100]; // quantity item show on a page
  public quantityDefault = 10;// quantity default 10
  public quantity = this.quantityDefault;
  public currentPage = 0;
  private getProductListUrl = URL.PRODUCT_DRAF;
  public listProduct = [];
  public maxPageShow = 5;
  public totalCount;
  public pageArr = [];
  public pageCount;
  public maxEntries;
  public minEntries;
  private currentSort = 'productId';
  public sortOption = {
    productId: true,
    productTitle: false,
    address: false,
    priceLow: false
  };
  private isAsc = true;
  public role = ROLE;
  private roleUser = ROLE_USER_TEXT;
  public pageNumber = [];
  public searchText = "";
  private timer;
  private currentDeleteId;
  constructor(
    private rest: RestService,
    private util: UtilService,
    private router: Router
  ) {
    this.getListProduct(this.currentPage, this.quantity);
  }

  ngOnInit() {
  }

  getListProduct(currentPage, quantity, searchText?) {
    this.util.showLoading();
    this.currentPage = currentPage;
    this.quantity = quantity;
    const url = this.util.convertUrl(this.getProductListUrl, currentPage, quantity, searchText);
    this.rest.GET(url, null, null, null, true).subscribe(res => {
      this.util.hideLoading();
      if (res && res.data && res.data.items) {
        this.listProduct = res.data.items;
        _.forEach(this.listProduct, (v, k) => {
          v['price'] = v['priceLow'] + ' - ' + v['pricehight'];
        })
        this.pageCount = res.data.pageCount;
        this.pageArr = this.util.generatePageArr(this.pageCount, this.maxPageShow, this.currentPage);
        this.totalCount = res.data.totalCount;
        this.minEntries = this.currentPage * this.quantity + 1;
        this.maxEntries = (this.currentPage + 1) * this.quantity > this.totalCount ? this.totalCount : (this.currentPage + 1) * this.quantity;
        // this.sort(this.currentSort);

      }
    }, err => {
      this.util.hideLoading();
      this.util.handleError();
    })
  }

  sort(type) {
    if (this.sortOption[type]) {
      this.isAsc = !this.isAsc;
      let option = this.isAsc ? 'asc' : 'desc';
      this.listProduct = _.orderBy(this.listProduct, [type], [option]);
    } else {
      this.sortOption[this.currentSort] = false;
      this.currentSort = type;
      this.sortOption[type] = true;
      this.listProduct = _.orderBy(this.listProduct, [type], ['asc']);
    }
  }

  search() {
    clearTimeout(this.timer);
    this.timer = setTimeout(() => {
      this.getListProduct(0, this.quantity, this.searchText);
    }, 500)
  }

  editProduct(data) {
    data.isDraft = true;
    this.util.setItemLocalStorage('CURRENT_PRODUCT', JSON.stringify(data));
    this.router.navigate(['/edit-product'], {queryParams: {draftId: data.productId}});
  }

  setCurrentDeleteProduct(id) {
    this.currentDeleteId = id;
  }
}
