import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailUserPartnerComponent } from './detail-user-partner.component';

describe('DetailUserPartnerComponent', () => {
  let component: DetailUserPartnerComponent;
  let fixture: ComponentFixture<DetailUserPartnerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailUserPartnerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailUserPartnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
