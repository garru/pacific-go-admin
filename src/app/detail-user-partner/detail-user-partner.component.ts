import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { RestService } from '../rest.service';
import * as _ from 'lodash';
import { UtilService } from '../util.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ValidationService } from '../shared/validation';
import { ROLE, ROLE_USER_TEXT, URL, TEXT, MODAL_ID, GENDER_TEXT, GENDER } from '../shared/constant';
declare var jQuery: any;
import * as moment from 'moment';
import 'chart.js';
import * as XLSX from 'xlsx';
import * as FileSaver from 'file-saver';
import { Workbook } from 'exceljs';
import * as Excel from "exceljs/dist/exceljs.min.js";
import * as ExcelProper from "exceljs";
import * as fs from 'file-saver';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-detail-user-partner',
  templateUrl: './detail-user-partner.component.html',
  styleUrls: ['./detail-user-partner.component.css']
})
export class DetailUserPartnerComponent implements OnInit {
	@ViewChild('timefromdate') timefromdate: ElementRef;
	@ViewChild('tcfromdate') tcfromdate: ElementRef;
	@ViewChild('tctodate') tctodate: ElementRef;
	@ViewChild('timetodate') timetodate: ElementRef;
	public quantityOption = [10, 25, 50, 100]; // quantity item show on a page
	public quantityDefault = 10;// quantity default 10
	public quantity = this.quantityDefault;
	public currentPage = 0;
	private getTransurl = URL.GET_TRANS_LIST_PARTNER;
	private getAmount = URL.GET_TRANS_AMOUNT;
	private getAmountMonth = URL.GET_TRANS_AMOUNT_MONTH;
	public listProduct = [];
	public maxPageShow = 5;
	public totalCount;
	public roleUserID;
	public pageArr = [];
	public pageCount;
	public maxEntries;
	public minEntries;
	private currentSort = 'productId';
	public sortOption = {
	productId: true,
	productTitle: false,
	address: false,
	priceLow: false
	};
	private isAsc = true;
	public role = ROLE;
	private roleUser = ROLE_USER_TEXT;
	public pageNumber = [];
	public searchText = "";
	private timer;
	private listrans = [];
	private lstransall = [];
	public createForm: FormGroup;
	public fromdate;
	public todate;
	public tcfromdateVal;
	public tctodateVal;
	public formData;
	private partnerID;
	public TotalAmount = 0;
	public selectedMonth = 0;
	public CurrentFromDate = 0;
	public CurrentToDate = 0;
	constructor(
		private formBuilder: FormBuilder,
		public validationService: ValidationService,
		private rest: RestService,
		private util: UtilService
		//private excelService: ExcelService
	) { 

	}

  	ngOnInit() {
  		this.createForm = this.formBuilder.group({
			timefromdate: ['', [Validators.required]],
			timetodate: ['', [Validators.required]],
			tcfromdate: ['', [Validators.required]],
			tctodate: ['', [Validators.required]],
		}, {  })

  		this.formData = JSON.parse(this.util.getItemLocalStorage('CURRENT_PARTNER'));
  		this.partnerID = this.formData['_id'];
  		this.roleUserID = this.formData['userRoleId'];
  		var nowdate = new Date();
  		this.fromdate = new Date((("0" + (nowdate.getMonth() + 1)).slice(-2))  + '/' + '01/' + nowdate.getFullYear()).getTime();
  		var pretime = nowdate.getTime() - (86400000 * 6)
  		this.todate = new Date().getTime();
  		this.todate += 86399000;
  		this.CurrentFromDate = this.fromdate;
  		this.CurrentToDate = this.todate;
  		this.getListrans(this.partnerID, this.CurrentFromDate, this.CurrentToDate, this.currentPage, this.quantity);
  		this.getListransChart(this.partnerID, pretime, 7);
  		this.loadMonth(0);
  	}

  	ngAfterContentInit() {
	    
	    jQuery(this.timefromdate.nativeElement).datepicker();
	    jQuery(this.timefromdate.nativeElement).on('change', () => {
	    	this.createForm.controls.timefromdate.setValue(jQuery(this.timefromdate.nativeElement).val());
      		this.createForm.controls.timefromdate.updateValueAndValidity();
	    })
	    
	    jQuery(this.timetodate.nativeElement).datepicker();
	    jQuery(this.timetodate.nativeElement).on('change', () => {
	    	this.createForm.controls.timetodate.setValue(jQuery(this.timetodate.nativeElement).val());
      		this.createForm.controls.timetodate.updateValueAndValidity();
	    })

	    jQuery(this.tcfromdate.nativeElement).datepicker();
	    jQuery(this.tcfromdate.nativeElement).on('change', () => {
	    	this.createForm.controls.tcfromdate.setValue(jQuery(this.tcfromdate.nativeElement).val());
      		this.createForm.controls.tcfromdate.updateValueAndValidity();

      		var getfromdate = new Date(this.createForm.controls.tcfromdate.value).getTime();
      		var newdate = 0;
      		var mydate = new Date();
      		newdate = (getfromdate) + (86400000 * 6)
      		mydate = new Date(newdate)
      		if(isNaN(getfromdate)){
      			this.tctodateVal = 'mm/dd/yyy';
      		}else{
      			this.tctodateVal = (mydate.getMonth() + 1)   + '/' + mydate.getDate()  + '/' + mydate.getFullYear();
      		}
	    })
	    
	  }

  	getListrans(partner_id, fromdate, todate, currentPage, quantity) {
	  	if(isNaN(todate) || isNaN(fromdate)){
			this.util.handleErrorDate();
		}else{
		    this.util.showLoading();
		    this.currentPage = currentPage;
		    this.quantity = quantity;
		    const url = this.util.convertUrlTrans(this.getTransurl, partner_id, fromdate, todate, currentPage, quantity);
		    
		    this.rest.GET(url, null, null, null, true).subscribe(res => {
		      this.util.hideLoading();
		      if (res && res.data && res.data.trans) {
		      	this.listrans = res.data.trans;
		      	this.lstransall = res.data.transall;
		      	this.pageCount = res.data.pageCount;
		        this.pageArr = this.util.generatePageArr(this.pageCount, this.maxPageShow, this.currentPage);
		        this.totalCount = res.data.totalCount;
		        this.minEntries = this.currentPage * this.quantity + 1;
		        this.maxEntries = (this.currentPage + 1) * this.quantity > this.totalCount ? this.totalCount : (this.currentPage + 1) * this.quantity;
		      }
		      
		    }, err => {
		      this.util.hideLoading();
		      this.util.handleError('', err ? err.message : '');
		    })
	   }
  	}

  	getListransChart(partner_id, fromdate, nextDayCount){
  		if(isNaN(fromdate)){
			this.util.handleErrorDate();
		}else{
			const url = this.getAmount+ '?partner_id='+ partner_id + '&fromdate=' + fromdate + '&nextDayCount='+ nextDayCount;
		    this.rest.GET(url, null, null, null, true).subscribe(res => {
		      if (res && res.data && res.data) {
		        var mydate = new Date();
		    	var newdate = new Date(fromdate);
		    	this.lineChartLabels = [];
		    	this.lineChartData[0].data = res.data.amount;
		    	this.lineChartLabels = res.data.nextDay;
		      }
		      
		    }, err => {
		      this.util.hideLoading();
		      this.util.handleError('', err ? err.message : '');
		    })
		}
  	}

  	pageGetListrans(currentPage, quantity){
  		this.getListrans(this.partnerID, this.CurrentFromDate, this.CurrentToDate, currentPage, quantity);
  	}

  	loadView(){
  		var locFromdate = new Date(this.createForm.controls.timefromdate.value).getTime();
  		var locTodate = new Date(this.createForm.controls.timetodate.value).getTime();
  		if(!isNaN(locFromdate) && !isNaN(locTodate)){
			this.CurrentFromDate = locFromdate;
			this.CurrentToDate = locTodate;
			this.currentPage = 0;
		}
  		this.getListrans(this.partnerID, locFromdate, locTodate, this.currentPage, this.quantity);
  	}
  	loadViewChart(){
  		var cvfromtimes = new Date(this.createForm.controls.tcfromdate.value).getTime();
  		this.getListransChart(this.partnerID, cvfromtimes, 7);
  	}

  	public lineChartData:Array<any> = [
	    {data: [0, 0, 0, 0 , 0, 0, 0], label: 'Tiền'},
  	];
  	public lineChartLabels:Array<any> = [];
	public lineChartOptions:any = {
		responsive: true
	};
	public lineChartColors:Array<any> = [
		{ 
		  backgroundColor: 'rgba(148,159,177,0.2)',
		  borderColor: 'rgba(148,159,177,1)',
		  pointBackgroundColor: 'rgba(148,159,177,1)',
		  pointBorderColor: '#fff',
		  pointHoverBackgroundColor: '#fff',
		  pointHoverBorderColor: 'rgba(148,159,177,0.8)'
		}
	];
	public lineChartLegend:boolean = true;
	public lineChartType:string = 'line';
	 
	public randomize():void {
		let _lineChartData:Array<any> = new Array(this.lineChartData.length);
		for (let i = 0; i < this.lineChartData.length; i++) {
			_lineChartData[i] = {data: new Array(this.lineChartData[i].data.length), label: this.lineChartData[i].label};
	  		for (let j = 0; j < this.lineChartData[i].data.length; j++) {
	    		_lineChartData[i].data[j] = Math.floor((Math.random() * 100) + 1);
	  		}
		}
		this.lineChartData = _lineChartData;
	}
	 
	// events
	public chartClicked(e:any):void {
		console.log(e);
	}
	 
	public chartHovered(e:any):void {
		console.log(e);
	}
	
	exportExcel(){
		if(this.lstransall.length == 0){
			this.util.handleDateNull();
			return;
		}

		const title = 'Danh sách các giao dịch';
		var header = [];
		if (this.roleUserID == 4){
			header = ["Đối tác", "Người giới thiệu", "Thành viên", "Khách hàng", "Số điểm", "Địa điểm", "Ngày"]
		}else{
			header = ["Đối tác", "Người giới thiệu", "Thành viên", "Khách hàng", "Số tiền", "Địa điểm", "Ngày"]
		}
		var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
		var vfromdate = new Date()
		var vtodate = new Date()
		if (this.createForm.controls.timefromdate.value){
			vfromdate = new Date(this.createForm.controls.timefromdate.value)
		}else{
			var nowdate = new Date();
  			vfromdate = new Date((("0" + (nowdate.getMonth() + 1)).slice(-2))  + '/' + '01/' + nowdate.getFullYear());
		}

		if (this.createForm.controls.timetodate.value){
			vtodate = new Date(this.createForm.controls.timetodate.value)
		}else{
			vtodate = new Date()
		}

		
		const titledate = [
							"Từ ngày",
							vfromdate.toLocaleDateString("en-US", options),
							"Đến ngày",
							vtodate.toLocaleDateString("en-US", options),
						  ]
		let workbook: ExcelProper.Workbook = new Excel.Workbook();
		let worksheet = workbook.addWorksheet('');
		let titleRow = worksheet.addRow([title]);
		// Set font, size and style in title row.
		titleRow.font = { name: 'Comic Sans MS', family: 4, size: 16, underline: 'double', bold: true };
		// Blank Row
		worksheet.addRow([]);
		//Add row with current date
		let subTitleRow = worksheet.addRow(titledate);
		let headerRow = worksheet.addRow(header);
		// Add Data and Conditional Formatting
		this.lstransall.forEach(d => {
				if(this.roleUserID == 4){
					d.amount = d.amount / 100000
				}
				//d.amount = Number.parseFloat(d.amount)
				d.amount = Number(d.amount)
				worksheet.addRow(Object.values(d));
	    	}
	    );
	    worksheet.getColumn(2).width = 30;
	    worksheet.getColumn(3).width = 30;
	    worksheet.getColumn(4).width = 30;
	    worksheet.addRow([]);

		headerRow.eachCell((cell, number) => {
	      cell.fill = {
	        type: 'pattern',
	        pattern: 'solid',
	        fgColor: { argb: 'FFFFFF00' },
	        bgColor: { argb: 'FF0000FF' }
	      }
	      cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
	    })
	    let footerRow = worksheet.addRow(['Cuối trang']);
		    footerRow.getCell(1).fill = {
	      type: 'pattern',
	      pattern: 'solid',
	      fgColor: { argb: 'FFCCFFE5' }
	    };
	    footerRow.getCell(1).border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }

	    worksheet.mergeCells(`A${footerRow.number}:F${footerRow.number}`);

		workbook.xlsx.writeBuffer().then((data : any) => {
		let blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
		fs.saveAs(blob, 'Transactions.xlsx');
		})
	}

	onChange(newValue) {
	    this.loadMonth(newValue);
	}

	formatNumber(num) {
	  return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
	}

	loadMonth(newValue){
		var current = new Date().getTime();
		const url = this.getAmountMonth+ '?partner_id='+ this.partnerID + '&current=' + current + '&month='+ newValue;
	    this.rest.GET(url, null, null, null, true).subscribe(res => {
	      if (res && res.data && res.data) {
	        this.TotalAmount = res.data.amount;
	      }
	      
	    }, err => {
	      this.util.hideLoading();
	      this.util.handleError('', err ? err.message : '');
	    })
	}

}
