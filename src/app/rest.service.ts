import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { UtilService } from './util.service';
import * as moment from 'moment';
import { KEY_STORAGE } from './shared/constant';
import { Router } from '@angular/router';

@Injectable()
export class RestService {
  public baseUrl = "https://pacificgo.asia/secret";
  //  public baseUrl = "http://localhost:5001/api/v1";
  constructor(
    private http: HttpClient,
    private util: UtilService,
    private router: Router
    ) { }

  GET(url, data = null, UrlbaseUrl?, header?, token?): Observable<any> {
    let loginTime = this.util.getItemLocalStorage(KEY_STORAGE.LOGIN_TIME);
    let expireTime = this.util.getItemLocalStorage(KEY_STORAGE.EXPIRY_TIME);
    if(expireTime && loginTime && token) {
      const currentTime = moment.utc();
      let diffTime = currentTime.diff(loginTime, 'second');
      if(diffTime >=  parseInt(expireTime)) {
        this.logout();
        return of(0);
      } else {
        this.util.setItemLocalStorage(KEY_STORAGE.LOGIN_TIME, currentTime);
        let headers = (token || header) ? this.getHeaderHttp(header, token) : null;
        return this.http.get(UrlbaseUrl ? UrlbaseUrl : this.baseUrl + url, { headers: headers, params: data })
          .pipe(map(res => {
            return res;
          })).pipe(catchError(this.handleError));
      }
    } else {
      if(!loginTime && token) {
          this.logout();
          return of(0);
        } else {
          let headers = (token || header) ? this.getHeaderHttp(header, token) : null;
          return this.http.get(UrlbaseUrl ? UrlbaseUrl : this.baseUrl + url, { headers: headers, params: data })
            .pipe(map(res => {
              return res;
            })).pipe(catchError(this.handleError));
        }
    }
  }

  POST(url, data = null, UrlbaseUrl?, header?, token?): Observable<any> {
    let loginTime = this.util.getItemLocalStorage(KEY_STORAGE.LOGIN_TIME);
    let expireTime = this.util.getItemLocalStorage(KEY_STORAGE.EXPIRY_TIME);
    if(expireTime && loginTime && token) {
      const currentTime = moment.utc();
      let diffTime = currentTime.diff(loginTime, 'second');
      if(diffTime >=  parseInt(expireTime)) {
        this.logout();
        return of(0);
      } else {
        this.util.setItemLocalStorage(KEY_STORAGE.LOGIN_TIME, currentTime);
        let options = { headers: this.getHeaderHttp(header, token) };
        return this.http.post(UrlbaseUrl ? UrlbaseUrl : this.baseUrl + url, data, options)
          .pipe(map(res => {
            return res;
          })).pipe(catchError(this.handleError));
      }
    } else {
      if(!loginTime && token) {
          this.logout();
          return of(0);
        } else {
          let options = { headers: this.getHeaderHttp(header, token) };
          return this.http.post(UrlbaseUrl ? UrlbaseUrl : this.baseUrl + url, data, options)
            .pipe(map(res => {
              return res;
            })).pipe(catchError(this.handleError));
        }
    }
  }

  PUT(url, data = null, UrlbaseUrl?, header?, token?): Observable<any> {
    let loginTime = this.util.getItemLocalStorage(KEY_STORAGE.LOGIN_TIME);
    let expireTime = this.util.getItemLocalStorage(KEY_STORAGE.EXPIRY_TIME);
    if(expireTime && loginTime && token) {
      const currentTime = moment.utc();
      let diffTime = currentTime.diff(loginTime, 'second');
      if(diffTime >=  parseInt(expireTime)) {
        this.logout();
        return of(0);
      } else {
        this.util.setItemLocalStorage(KEY_STORAGE.LOGIN_TIME, currentTime);
        let options = { headers: this.getHeaderHttp(header, token) };
        return this.http.put(UrlbaseUrl ? UrlbaseUrl : this.baseUrl + url, data, options)
          .pipe(map(res => {
            return res;
          })).pipe(catchError(this.handleError));
      }
    } else {
      if(!loginTime && token) {
          this.logout();
          return of(0);
        } else {
          let options = { headers: this.getHeaderHttp(header, token) };
          return this.http.put(UrlbaseUrl ? UrlbaseUrl : this.baseUrl + url, data, options)
            .pipe(map(res => {
              return res;
            })).pipe(catchError(this.handleError));
        }
    }
    
  }

  DELETE(url, data = null, UrlbaseUrl?, header?, token?): Observable<any> {
    let loginTime = this.util.getItemLocalStorage(KEY_STORAGE.LOGIN_TIME);
    let expireTime = this.util.getItemLocalStorage(KEY_STORAGE.LOGIN_TIME);
    if(expireTime && loginTime && token) {
      const currentTime = moment.utc();
      let diffTime = currentTime.diff(loginTime, 'second');
      if(diffTime >=  parseInt(expireTime)) {
        this.logout();
        return of(0);
      } else {
        this.util.setItemLocalStorage(KEY_STORAGE.LOGIN_TIME, currentTime);
        let options = { headers: this.getHeaderHttp(header, token) };
        return this.http.delete(UrlbaseUrl ? UrlbaseUrl : this.baseUrl + url, options)
          .pipe(map(res => {
            return res;
          })).pipe(catchError(this.handleError));
      }
    } else {
      if(!loginTime && token) {
          this.logout();
          return of(0);
        } else {
          let options = { headers: this.getHeaderHttp(header, token) };
          return this.http.delete(UrlbaseUrl ? UrlbaseUrl : this.baseUrl + url, options)
            .pipe(map(res => {
              return res;
            })).pipe(catchError(this.handleError));
        }
    }
  }
  
  getHeaderHttp(header = null, token?) {
    let headerObj = {
      'Content-Type': 'application/json',
    };

    if (token) {
      headerObj['Authorization'] = "Bearer " + this.getLocalStorage('pacific_token');
    }

    Object.assign(headerObj, header);
    return new HttpHeaders(headerObj);
  }

  handleError(error: HttpErrorResponse) {
    return throwError(error.message || "Server Error");
  }

  setLocalStorage(key, value) {
    localStorage.setItem(key, value);
  }

  getLocalStorage(key) {
    return localStorage.getItem(key);
  }
  removeLocalStorage(key) {
    localStorage.removeItem(key);
  }
  logout() {
    this.util.removeItemLocalStorage(KEY_STORAGE.EXPIRY_TIME);
    this.util.removeItemLocalStorage(KEY_STORAGE.LOGIN_TIME);
    this.util.removeItemLocalStorage(KEY_STORAGE.TOKEN);
    this.util.removeItemLocalStorage(KEY_STORAGE.USER_INFOR);
    window.location.pathname = "";
  }
}
