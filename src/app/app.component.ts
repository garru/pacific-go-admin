import { Component } from '@angular/core';
import { UtilService } from './util.service';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import {URL, TEXT, KEY_STORAGE }from './shared/constant'; 
import * as moment from 'moment'; 

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'pacific-go-admin';
  public userInfo = {};
  public isLogin = false;
  private pathNoHeader = ['/login'];
  constructor(
    private util: UtilService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {
    // this.router.events.pipe(
    //   filter(event => event instanceof NavigationEnd)  
    // ).subscribe((event: NavigationEnd) => {
    //   if(this.pathNoHeader.includes(event.url)) {
    //     this.isLogin = true;
    //   } else {
    //     this.isLogin = false;
    //   }
    // });
    // let loginTime = this.util.getItemLocalStorage(KEY_STORAGE.LOGIN_TIME); 
    // let expireTime = this.util.getItemLocalStorage(KEY_STORAGE.LOGIN_TIME); 
    // if ( ! loginTime) {
    //   this.router.navigate(['/login']);
    //   this.isLogin = true;
    // }else {
    //   const currentTime = moment.utc(); 
    //   let diffTime = currentTime.diff(loginTime, 'second'); 
    //   if (diffTime >= parseInt(expireTime)) {
    //     this.router.navigate(['/login']);
    //       this.isLogin = true;
    //   }
    // }
    // const userInfor = this.util.getUserInfor();
    // if (!userInfor) {
    //   this.router.navigate(['/login']);
    //   this.isLogin = true;
    // } else {
    //   this.router.navigate([''])
    // }
    this.router.navigate([''])
  }

  closeModal() {
    this.util.closerErrorModal();
  }
}
