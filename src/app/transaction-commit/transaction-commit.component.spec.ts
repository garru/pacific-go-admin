import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransactionCommitComponent } from './transaction-commit.component';

describe('TransactionCommitComponent', () => {
  let component: TransactionCommitComponent;
  let fixture: ComponentFixture<TransactionCommitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransactionCommitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionCommitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
