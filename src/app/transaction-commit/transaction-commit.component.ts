import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { RestService } from '../rest.service';
import * as _ from 'lodash';
import { UtilService } from '../util.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ValidationService } from '../shared/validation';
import { ROLE, ROLE_USER_TEXT, URL, TEXT, MODAL_ID, GENDER_TEXT, GENDER } from '../shared/constant';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
declare var jQuery: any;
import * as moment from 'moment';
import 'chart.js';

@Component({
  selector: 'app-transaction-commit',
  templateUrl: './transaction-commit.component.html',
  styleUrls: ['./transaction-commit.component.css']
})
export class TransactionCommitComponent implements OnInit {
	public quantityOption = [10, 25, 50, 100]; // quantity item show on a page
	public quantityDefault = 10;// quantity default 10
	public quantity = this.quantityDefault;
	private getTransUrl = URL.GET_TRANS_LIST;
	public listTrans = [];
	public currentPage = 0;
	public maxPageShow = 5;
	public totalCount;
	public pageArr = [];
	public pageCount;
	public maxEntries;
	public minEntries;
	public pageNumber = [];
	public searchText = "";
	private timer;
	private isAsc = true;
	private currentSort = '_id';
	public sortOption = {
	_id: true,
	member: false,
	customer: false,
	introducer: false,
	amount: false,
	address: true,
	desc: true,
	created_at: true,
	};
	public amount = 0;

  	constructor(
  				private formBuilder: FormBuilder,
			    public validationService: ValidationService,
			    private rest: RestService,
			    private util: UtilService,
			    private router: Router,
			    private route: ActivatedRoute
	    ) { 
	    	this.getTransaction(this.currentPage, this.quantity);
	    }

  	ngOnInit() {

  	}
  	
  	getTransaction(currentPage, quantity, searchText?) {
	    this.util.showLoading();
	    this.currentPage = currentPage;
	    this.quantity = quantity;
	    const url = this.util.convertUrlHistory(this.getTransUrl, currentPage, quantity, searchText);
	    this.rest.GET(url, null, null, null, true).subscribe(res => {
	      this.util.hideLoading();
	      if (res && res.data && res.data.items) {
	        this.listTrans = res.data.items;
	        this.pageCount = res.data.pageCount;
	        this.pageArr = this.util.generatePageArr(this.pageCount, this.maxPageShow, this.currentPage);
	        this.totalCount = res.data.totalCount;
	        this.minEntries = this.currentPage * this.quantity + 1;
	        this.maxEntries = (this.currentPage + 1) * this.quantity > this.totalCount ? this.totalCount : (this.currentPage + 1) * this.quantity;
	        if(res.data.amount == ''){
	        	this.amount = 0
	        }else{
	        	this.amount = res.data.amount
	        }
	        
	        // this.sort(this.currentSort, true);
	      }
	    }, err => {
	      this.util.hideLoading();
	      this.util.handleError('', err ? err.message : '');
	    })
  	}

  	search() {
		clearTimeout(this.timer);
		this.timer = setTimeout(() => {
		  this.getTransaction(0, this.quantity, this.searchText);
		}, 500)
	}

	sort(type, isResort?) {
	    isResort && (this.isAsc = !this.isAsc);
	    if (this.sortOption[type]) {
	      this.isAsc = !this.isAsc;
	      let option = this.isAsc ? 'asc' : 'desc';
	      this.listTrans = _.orderBy(this.listTrans, [type], [option]);
	    } else {
	      this.sortOption[this.currentSort] = false;
	      this.currentSort = type;
	      this.sortOption[type] = true;
	      this.listTrans = _.orderBy(this.listTrans, [type], ['asc']);
	    }
  	}

}
